<header style="background-image:url('https://www.afes.fr/wp-content/uploads/2019/01/logo_AFB.png'); height:100px; width:100%; background-position:center; background-repeat:no-repeat; margin-top:20px; margin-bottom:80px; position:relative">
    <h1 style="position:absolute; bottom:-80px; left:0; right:0; text-align:center">Datavisualisation</h1>
</header>

Le projet de datavisualisation a été initié par les membres de l'AFB (Agence Française pour la Biodiversité) basés à Orléans. L'objectif est de consulter graphiquement un grand nombre de données sur différents indicateurs liés à la diversité biologique. Pour y parvenir, ils ont formulé une demande de développement d'un tableau de bord développé avec les technologies web.


## Table des matières
- [Installation](/docs/installation.md)
- [Liens utils](/README.md#liens-utils)


## Liens utils
- SQLite ([documentation](https://www.sqlite.org/index.html, "Documentation SQLite"))
- Flask ([documentation](http://flask.pocoo.org/, "Documentation Flask"))
- SQLAlchemy ([documentation](https://www.sqlalchemy.org/, "Documentation SQLAlchemy"))
- vanilla js ([documentation](http://vanilla-js.com/, "Documentation vanilla js"))
- ApexChart.js ([documentation](https://apexcharts.com/, "Documentation ApexChart.js"))
- Leaflet ([documentation](https://leafletjs.com/, "Documentation Leaflet"))
- JSON ([documentation](https://www.json.org/, "Documentation JSON"))
- GeoJSON ([documentation](http://geojson.org/, "Documentation GeoJSON"))