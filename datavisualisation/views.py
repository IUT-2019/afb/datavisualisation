
#### IMPORT #################
from .app import app, login_manager
from .services.csvFile import *
from .services import charts, tables, maps
from .models import *
from .breadcrumbs import *
from .forms import Login, ImportData
from flask import render_template, url_for, request, redirect
from flask_login import login_user, current_user, logout_user, login_required
from flask_breadcrumbs import register_breadcrumb
from functools import wraps
from datetime import datetime
import json




#### DECORATEUR #############
def stopNavigateur( f ):
    @wraps( f )
    def decorateur_function( *args, **kargs ):
        if ( request.user_agent.browser.lower() != "firefox"):
            return render_template(
                "includes/navigateur.html",
                title = app.config["TITLE"]
            )
        return f( *args, **kargs )
    return decorateur_function




#### ROUTES #################
#### ACCUEIL
@app.route("/")
@register_breadcrumb(app, '.', 'Accueil')
@stopNavigateur
def accueil():
    return render_template(
        "includes/accueil.html",
        title = app.config["TITLE"],
        description = app.config["DESCRIPTION"],
        jeuxDeDonnees = getAllData(),
        formLogin = Login()
        # A compléter
    )




############################################################
#### NOUVELLES DONNEES
@login_required
@app.route("/nouvelles-données/")
@stopNavigateur
def nouvellesDonnees():
    return render_template(
        "includes/administration.html",
        title = app.config["TITLE"],
        formImportData = ImportData(),
        formLogin = Login()
        # A compléter
    )


@login_required
@app.route("/nouvelles-données/add/", methods=["POST"])
def addNouvellesDonnees():
    formImportData = ImportData()
    validity = formImportData.isValide()
    if( validity["valide"] ):
        uploadCsvFile( formImportData )
    return redirect( request.referrer )





############################################################
#### VISUALISATION
# Static
@app.route("/visualisation/<int:id>/statistique/")
@register_breadcrumb(app, ".loadDataStatistique", "", dynamic_list_constructor=breadcrumbStatistique)
@stopNavigateur
def visualisationStatistique(id):
    data = getData( id )
    csvFile = loadCsvFile( id )
    date = datetime.now()
    currentYear = date.year
    currentMonth = date.month
    return render_template(
        "views/" + data.template + "/contents/statistique.html",
        title = app.config["TITLE"],
        data = data,
        currentYear = currentYear,
        saisieCetteAnnee = SaisiesAnnuel( csvFile, currentYear, currentMonth ),
        saisieAnneePrecedente = SaisiesAnnuel( csvFile, currentYear-1, currentMonth ),
        nbDepartement = numberDepartement( csvFile ),
        nbRegion = numberRegion( csvFile ),
        formLogin = Login()
        # A compléter
    )




# Jeu de données
@app.route("/visualisation/<int:id>/jeux-de-données/")
@stopNavigateur
def visualisationJeuxDeDonnees(id):
    data = getData( id )
    return render_template(
        "views/" + data.template + "/contents/jeuDeDonnees.html",
        title = app.config["TITLE"],
        file = loadCsvFile( id ),
        data = data,
        formLogin = Login()
        # A compléter
    )




# Opérations par chronologie
@app.route("/visualisation/<int:id>/chronologie/")
@register_breadcrumb(app, ".visualisationChronologie", "", dynamic_list_constructor=breadcrumbChronologie)
@stopNavigateur
def visualisationChronologie( id ):
    data = getData( id )
    csvFile = loadCsvFile( id )
    return render_template(
        "views/" + data.template + "/contents/saisies/chronologie.html",
        title = app.config["TITLE"],
        data = data,
        listYear = listYear( csvFile ),
        formLogin = Login()
    )

@app.route("/visualisation/<int:id>/chronologie/data/")
def loadDataChronologie( id ):
    csvFile = loadCsvFile( id )
    out = {
        "data": json.dumps( csvFile ),
        "seriesOperationsParMois": charts.comparaisonChronologique.operations( csvFile ),
        "seriesOperationsHistorique": charts.historique.operations( csvFile )
    }
    return json.dumps( out )




# Opérations par géographie: région
@app.route("/visualisation/<int:id>/géographie/région/")
@register_breadcrumb(app, ".visualisationGeographieRegion", "", dynamic_list_constructor=breadcrumbGeographieRegion)
@stopNavigateur
def visualisationGeographieRegion(id):
    data = getData( id )
    csvFile = loadCsvFile( id )
    date = datetime.now()
    currentYear = date.year
    return render_template(
        "views/" + data.template + "/contents/saisies/geographie/region.html",
        title = app.config["TITLE"],
        data = data,
        currentYear = currentYear,
        minYear = minYear( csvFile ),
        tabRegionAnneeCourante = tables.operationsParRegions.tableauAnneeEnCours( csvFile ),
        formLogin = Login()
    )

@app.route("/visualisation/<int:id>/géographie/région/data/")
def loadDataGeographieRegion(id):
    csvFile = loadCsvFile( id )
    out = {
        "seriesOperationsParRegionsComparaisonMensuelle": charts.comparaisonGeographique.region.comparaisonMensuelle( csvFile ),
        "pourcentageRegionsAnneeEnCours": maps.operationsParRegions.pourcentageAnneeEnCours( csvFile ),
        "regionsCumulMensuel": charts.cumul.region.cumulMensuel( csvFile )
    }
    return json.dumps( out )




# Opérations par géographie: département
@app.route("/visualisation/<int:id>/géographie/département/")
@register_breadcrumb(app, ".visualisationGeographieDepartement", "", dynamic_list_constructor=breadcrumbGeographieDepartement)
@stopNavigateur
def visualisationGeographieDepartement(id):
    data = getData( id )
    csvFile = loadCsvFile( id )
    date = datetime.now()
    currentYear = date.year
    return render_template(
        "views/" + data.template + "/contents/saisies/geographie/departement.html",
        title = app.config["TITLE"],
        data = data,
        currentYear = currentYear,
        minYear = minYear( csvFile ),
        tabDepartementAnneeCourante = tables.operationsParDepartements.tableauAnneeEnCours( csvFile ),
        formLogin = Login()
    )

@app.route("/visualisation/<int:id>/géographie/département/data/")
def loadDataGeographieDepartement(id):
    csvFile = loadCsvFile( id )
    out = {
        "seriesOperationsParDepartementsComparaisonMensuelle": charts.comparaisonGeographique.departement.comparaisonMensuelle( csvFile ),
        "pourcentageDepartementsAnneeEnCours": maps.operationsParDepartements.pourcentageAnneeEnCours( csvFile ),
        "departementsCumulMensuel": charts.cumul.departement.cumulMensuel( csvFile )
    }
    return json.dumps( out )




# Espèces - Chronologie
@app.route("/visualisation/<int:id>/espèces/chronologie/")
@register_breadcrumb(app, ".visualisationEspecesChronologie", "", dynamic_list_constructor=breadcrumbEspeceChronologie)
@stopNavigateur
def visualisationEspecesChronologie( id ):
    data = getData( id )
    csvFile = loadCsvFile( id )
    return render_template(
        "views/" + data.template + "/contents/especes/chronologie.html",
        title = app.config["TITLE"],
        data = data,
        listYear = listYear( csvFile ),
        formLogin = Login()
    )

@app.route("/visualisation/<int:id>/espèces/chronologie/data/")
def loadDataEspecesChronologie( id ):
    csvFile = loadCsvFile( id )
    out = {
        "data": json.dumps( csvFile ),
        "seriesOperationsParMois": charts.comparaisonChronologique.especesDifferentes( csvFile ),
        "seriesOperationsHistorique": charts.historique.operations( csvFile )
    }
    return json.dumps( out )




# historique comparatif
@app.route("/visualition/historique-comparatif/")
@register_breadcrumb(app, ".historiqueComparatif", "", dynamic_list_constructor=breadcrumbHisoriqueComparatif)
@stopNavigateur
def historiqueComparatif():
    return render_template(
        "views/historiqueComparatif/index.html",
        title = app.config["TITLE"],
        formLogin = Login()
    )

@app.route("/visualition/historique-comparatif/data/")
def loadHistoriqueComparatif():
    out = {"seriesOperationsElementaires":charts.evolutionRelative.operations()}
    return json.dumps( out )




############################################################
#### LOGIN / LOGOUT
@login_manager.user_loader
def user_loader(user_id):
    return Administrateur.query.get(user_id)



@app.route("/login/", methods=["POST"])
def login():
    formLogin = Login()
    user = formLogin.isAuthentified()
    if user and login_user(user):
        user_loader(user.id)
        return redirect(url_for("nouvellesDonnees"))
    return redirect(request.referrer)


@login_required
@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for("accueil"))