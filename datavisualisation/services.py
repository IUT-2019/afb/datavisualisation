

#### IMPORT #################
from .app import app
from .models import *
from .forms import *
import csv
import json




#### METHODS ################
def uploadCsvFile( form ):
    """
        Enregistre sur le serveur dans le répertoire "upload/database" une base de données
        relationnel exporté dans un format .csv.
        :param form: Un formulaire
        :type file: Object
        :return: Un indicateur de bonne execution de la méthode
        :rtype: Boolean
    """
    file = form.csvFile.data
    try:
        filename = secure_filename( file.filename )
        file.save( app.config["DIRECTORY_CSV"] + filename )
        addCarte("csv", filename, filename, None, None, None)
    except:
        return False
    return True


def loadCsvFile( id ):
    """
        Charge les données (au format JSON) d'un fichier .csv donné.
        :param id: L'identifiant du fichier CSV
        :type id: int
        :return: Un Objet de données formaté en JSON
        :rtype: json
    """
    database = getCarte( id )
    with open(file=app.config["DIRECTORY_GEOJSON"]+database.nom, mode="rU", encoding="utf-8") as file:
        reader = csv.DictReader(file, delimiter=';', quotechar="\"")
        data = list()
        for row in reader:
            data.append( row )
        return json.dumps( data )