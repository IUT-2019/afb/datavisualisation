

#### IMPORT #################
from ..app import app
from ..models import *
import csv
import json
from datetime import datetime
from werkzeug.utils import secure_filename
from subprocess import run




#### METHODS ################
def loadCsvFile( id ):
    """
        Charge les données d'un fichier .csv donné.
        :param id: L'identifiant du fichier CSV
        :type id: int
        :return: Les données du fichier
        :rtype: dict
    """
    database = getData( id )
    with open(file=app.config["DIRECTORY_CSV"]+database.nom, mode="rU", encoding="utf-8") as file:
        reader = csv.DictReader(file, delimiter=';', quotechar="\"")
        data = dict()
        for row in reader:
            value = dict(row)
            data[value["id"]] = value
        return data


def uploadCsvFile( form ):
    """
        Enregistre un jeu de données .csv
        :param form: Le formulaire d'ajout de csv
        :type form: ImportData
    """
    titre = form.titre.data
    nom = form.nom.data
    description = form.description.data
    package = form.package.data
    file = form.csvFile.data
    packagename = app.config["DIRECTORY_PACKAGES"] + secure_filename( package.filename )
    filename = app.config["DIRECTORY_TEMPORAIRE"] + secure_filename( "mappingInput.csv" )
    try:
        file.save( filename )
        package.save( packagename )
    except OSError as error:
        print( error )
    else:
        try:
            commande = [ app.config["JAVA"], "-jar", packagename ]
            run( commande )
        except OSError as error:
            print( error )
        else:
            try:
                mappingOut = app.config["DIRECTORY_CSV"] + "mappingOut.csv"
                newFile = app.config["DIRECTORY_CSV"] + nom
                commande = [ "mv", mappingOut, newFile ]
                run( commande )
            except OSError as error:
                print( error )
            else:
                addData(titre=titre, nom=nom, package=package.filename, description=description)
    finally:
        commande = [ "rm", filename ]
        run( commande )


def maxYear( data ):
    """
        Trouve l'année la plus récente.
        :param data: Les données d'un fichier csv
        :type data: dict
        :return: L'année la plus grande
        : rtype: int
    """
    s = max(data, key=lambda x: data[x]["date"])
    d = datetime.strptime(data[s]["date"], "%Y-%m-%d")
    return d.year


def minYear( data ):
    """
        Trouve l'année la plus ancienne.
        :param data: Les données d'un fichier csv
        :type data: dict
        :return: L'année la plus petite
        : rtype: int
    """
    s = min(data, key=lambda x: data[x]["date"])
    d = datetime.strptime(data[s]["date"], "%Y-%m-%d")
    return d.year


def numberDepartement( data ):
    """
        Calcul le nombre de département dans le jeu de données.
        :param data: Les données d'un fichier csv
        :type data: dict
        :return: le nombre de département
        : rtype: int
    """
    departements = set()
    for id in data:
        departements.add( data[id]["code_departement"] )
    return len( departements )


def numberRegion( data ):
    """
        Calcul le nombre de régions dans le jeu de données.
        :param data: Les données d'un fichier csv
        :type data: dict
        :return: Le nombre de régions
        : rtype: int
    """
    regions = set()
    for id in data:
        try:
            regions.add( data[id]["code_region"] )
        except:
            break
    return len( regions )


def SaisiesAnnuel( data, year, month ):
    """
        Calcul le nombre de saisie de l'année saisie.
        :param data: Les données d'un fichier csv
        :param year: Un année
        :param month: Un mois
        :type data: dict
        :type year: int
        :type month: int
        :return: Un nombre d'apparition pour l'année de janvier au mois précisé.
        :rtype: list
    """
    cpt = 0
    for id in data:
        date = datetime.strptime(data[id]["date"], "%Y-%m-%d")
        currentyear = date.year
        currentmonth = date.month
        if(currentyear == year and currentmonth <= month):
            cpt += 1
    return cpt




def listYear( data ):
    """
        Détermine une liste des années représenté.
        :param data: Les données d'un fichier csv
        :type data: dict
        :return: La liste des années trié dans l'ordre
        : rtype: list
    """
    years = set()
    for id in data:
        date = datetime.strptime(data[id]["date"], "%Y-%m-%d")
        years.add( date.year )
    return sorted(list( years ), reverse=True)