

#### IMPORT #################
from datetime import datetime




#### METHODS ################
def tableauAnneeEnCours( csvFile ):
    """
        Liste le nombre d'opérations effectué par régions pour cette année.
        :param csvFile: le jeu de données
        :type csvFile: dict
        :return: la liste des régions avec leurs valeurs absolut et relative.
        :rtype: dict
    """
    output = {"data":[], "invalide":None}
    regions = dict()
    nbOperations = 0
    nbOperationsNonUtilisable = 0
    currentYear = datetime.today().year
    for value in csvFile.values():
        valueYear = datetime.strptime(value["date"], "%Y-%m-%d").year
        if( currentYear == valueYear ):
            try:
                code = value["code_region"]
                if( code == "" ):
                    nbOperationsNonUtilisable += 1
                    continue
            except:
                nbOperationsNonUtilisable += 1
                continue
            else:
                if( code not in regions ):
                    regions[code] = {
                        "nom": value["nom_region"],
                        "operations": 0,
                        "pourcentage": 0
                    }
                regions[code]["operations"] += 1
                nbOperations += 1
    if( len(regions) != 0 ):
        for value in regions.values():
            value["pourcentage"] = round( value["operations"] * 100 / nbOperations, 2 )
            output["data"].append( value )
        output["data"] = sorted(output["data"], key=lambda oprt: oprt["operations"], reverse=True)
        if( nbOperationsNonUtilisable != 0 ):
            output["invalide"] = round( nbOperationsNonUtilisable * 100 / len( csvFile ), 2 )
    return output