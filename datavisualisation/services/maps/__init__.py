

#### IMPORT #################
import sys




#### TOP ACCESS #############
sys.path.append("../..")




#### FACTORIZATION ##########
from .operationsParRegions import classementAnneeEnCours, pourcentageAnneeEnCours
from .operationsParDepartements import classementAnneeEnCours, pourcentageAnneeEnCours