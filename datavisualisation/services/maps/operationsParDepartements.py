

#### IMPORT #################
from datetime import datetime




#### METHODS ################
def classementAnneeEnCours( csvFile ):
    """
        Liste des régions par classement en fonction du nombre d'opération
        effectué sur l'année en cours
        :param csvFile: le jeu de données
        :type csvFile: dict
        :return: une liste de liste des codes départements.
        :rtype: dict
    """
    output = {"data":[], "invalide":None}
    departements = dict()
    classes = dict()
    nbOperationsNonUtilisable = 0
    currentYear = datetime.today().year
    
    for value in csvFile.values():
        valueYear = datetime.strptime(value["date"], "%Y-%m-%d").year
        if( currentYear == valueYear ):
            try:
                code = value["code_departement"]
                if( code == "" ):
                    nbOperationsNonUtilisable += 1
                    continue
            except:
                nbOperationsNonUtilisable += 1
                continue
            else:
                if( code not in departements ):
                    departements[code] = 0
                departements[code] += 1
    
    for code, value in departements.items():
        valuesString = str( value )
        if( valuesString not in classes ):
            classes[valuesString] = list()
        classes[valuesString].append( code )

    output["data"] = [ classes[key] for key in sorted( classes, key=lambda x: int(x), reverse=True ) ]
    output["invalide"] = round( nbOperationsNonUtilisable * 100 / len( csvFile ), 2 )
    return output




def pourcentageAnneeEnCours( csvFile ):
    """
        Donne le pourcentage d'operations pour chaque départements durant
        l'année en cours.
        :param csvFile: le jeu de données
        :type csvFile: dict
        :return: une liste de liste des codes régions.
        :rtype: dict
    """
    output = {"data":dict(), "invalide":None}
    departements = dict()
    nbOperations = 0
    nbOperationsNonUtilisable = 0
    currentYear = datetime.today().year

    for value in csvFile.values():
        valueYear = datetime.strptime(value["date"], "%Y-%m-%d").year
        if( currentYear == valueYear ):
            try:
                code = value["code_departement"]
                if( code == "" ):
                    nbOperationsNonUtilisable += 1
                    continue
                if( len(code) == 1 ):
                    code = "0" + code
            except:
                nbOperationsNonUtilisable += 1
                continue
            else:
                if( code not in departements ):
                    departements[code] = 0
                departements[code] += 1
                nbOperations += 1
    if( len(departements) != 0 ):
        for code, value in departements.items():
            output["data"][code] = round( value * 100 / nbOperations, 2 )
        if( nbOperationsNonUtilisable != 0 ):
            output["invalide"] = round( nbOperationsNonUtilisable * 100 / len( csvFile ), 2 )
    return output
