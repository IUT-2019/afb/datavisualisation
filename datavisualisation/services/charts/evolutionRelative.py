

#### IMPORT #################
from ...models.Data import getAllData
from ..csvFile import loadCsvFile
from datetime import datetime
import threading




#### METHODS ################
def operations():
    """
        Pour chaque jeux de données, on détermine le nombre d'opérations
        annuelles sur les 10 dernières années.
        :return: les séries de chaque jeux de données
        :rtype: list
    """
    listData = getAllData()
    listThread = list()
    series = list()
    for data in listData:
        serie = {"name": data.titre, "data": []}
        series.append( serie )
        thread = threading.Thread(target=worker, args=(data.id, serie["data"]))
        listThread.append( thread )
    for thread in listThread:
        thread.start()
    for thread in listThread:
        thread.join()
    return series




def worker(fileId, output):
    """
        Calcul le nombre d'opérations mensuelles pour un seul jeu de données
        dont l'identifiant est passé en paramètre.
        :param fileId: l'identifiant BDD du jeu de données
        :param output: l'ensemble des observations pour un seul jeu de données (série)
        :param fileId: number
        :param output: list
    """
    lastYear = datetime.today().year
    fisrtYear = lastYear - 9
    dictData = loadCsvFile( fileId )
    series = dict()
    for year in range(fisrtYear, lastYear+1):
        series[str(year)] = {"x":year, "y":None}
    for key in dictData:
        year = datetime.strptime(dictData[key]["date"], "%Y-%m-%d").year
        if( fisrtYear <= year <= lastYear ):
            if( series[str(year)]["y"] == None ):
                series[str(year)]["y"] = 0
            series[str(year)]["y"] += 1
    for _, value in series.items():
        output.append( value )