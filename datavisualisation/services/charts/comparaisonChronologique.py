

#### IMPORT #################
from datetime import datetime




#### METHODS ################
def operations( csvFile ):
    output = { "data": list(), "invalide": None }
    series = dict()
    currentYear = datetime.today().year        
    for value in csvFile.values():
        valueDate = datetime.strptime(value["date"], "%Y-%m-%d")
        valueMonth = valueDate.month - 1
        valueYear = valueDate.year
        valueYearString = str( valueYear )
        if( currentYear - 1 <= valueYear <= currentYear ):
            if( valueYearString not in series ):
                series[valueYearString] = {"name":valueYear, "data":[None]*12}
            if( series[valueYearString]["data"][valueMonth] == None ):
                series[valueYearString]["data"][valueMonth] = 0
            series[valueYearString]["data"][valueMonth] += 1
    for value in series.values():
        output["data"].append( value )
    return output




def especesDifferentes( csvFile ):
    output = { "data": list(), "invalide": None }
    series = dict()
    nbNonUtilisable = 0
    currentYear = datetime.today().year        
    for value in csvFile.values():
        valueDate = datetime.strptime(value["date"], "%Y-%m-%d")
        valueMonth = valueDate.month - 1
        valueYear = valueDate.year
        valueYearString = str( valueYear )
        if( currentYear - 1 <= valueYear <= currentYear ):
            codeEspece = value["code_espece"]
            if( codeEspece == "" ):
                nbNonUtilisable += 1
                continue
            else:
                if( valueYearString not in series ):
                    series[valueYearString] = {"name":valueYear, "data":[None]*12}
                if( series[valueYearString]["data"][valueMonth] == None ):
                    series[valueYearString]["data"][valueMonth] = { codeEspece }
                series[valueYearString]["data"][valueMonth].add( codeEspece )
    for value in series.values():
        for i in range( len(value["data"]) ):
            month = value["data"][i]
            if( month is not None ):
                value["data"][i] = len( month )
        output["data"].append( value )
    if( nbNonUtilisable != 0 ):
        output["invalide"] = round(nbNonUtilisable * 100 / len( csvFile ), 2)
    return output