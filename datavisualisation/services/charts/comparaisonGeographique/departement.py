

#### IMPORT #################
from datetime import datetime




#### METHODS ################
def comparaisonMensuelle( csvFile ):
    """
        Calcul le nombre d'opération mensuel sur l'année en cours et l'année
        précédente pour réaliser une comparaison de production.
        :param csvFile: le jeu de données
        :type csvFile: dict
        :return: les séries de l'année en cours et de la précédente
        :rtype: dict
    """
    output = {
        "data": {
            "series": dict(),
            "codes": list(),
            "previous": None
        },
        "invalide": None
    }
    currentDate = datetime.today()
    currentYear = currentDate.year
    currentMonth = currentDate.month - 2
    previousYear = currentYear - 1
    currentYearString = str( currentYear )
    previousYearString = str( previousYear )
    nbOperationsNonUtilisable = 0
    series = dict()
    for value in csvFile.values():
        try:
            valueDate = datetime.strptime(value["date"], "%Y-%m-%d")
            valueMonth = valueDate.month - 1
            valueYear = valueDate.year
            valueYearString = str( valueYear )
        except ValueError as error:
            nbOperationsNonUtilisable += 1
            continue
        else:
            if( previousYear <= valueYear <= currentYear ):
                try:
                    valueCode = value["code_departement"]
                    valueNom = value["nom_departement"]
                    if( valueCode == "" ):
                        nbOperationsNonUtilisable += 1
                        continue
                    if( len(valueCode) == 1 ):
                        valueCode = "0" + valueCode
                except ValueError as error:
                    nbOperationsNonUtilisable += 1
                    continue
                else:
                    if( valueCode not in series ):
                        series[valueCode] = {
                            previousYearString: {"name": valueNom + " (" + previousYearString + ")", "data": [0] * 12},
                            currentYearString: {"name": valueNom + " (" + currentYearString + ")", "data": [0] * currentMonth + [None] * (12 - currentMonth)}
                        }
                    if( series[valueCode][valueYearString]["data"][valueMonth] == None ):
                        series[valueCode][valueYearString]["data"][valueMonth] = 0
                    series[valueCode][valueYearString]["data"][valueMonth] += 1
    output["data"]["series"] = series
    output["data"]["codes"] = list( series )
    output["data"]["previous"] = output["data"]["codes"][0]
    output["invalide"] = nbOperationsNonUtilisable
    return output