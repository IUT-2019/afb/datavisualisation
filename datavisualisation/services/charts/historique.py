

#### IMPORT #################
from datetime import datetime




#### METHODS ################
def operations( csvFile ):
    serie = {"name":"Historique", "data":[]}
    data = dict()
    for _, value in csvFile.items():
        valueDate = datetime.strptime(value["date"], "%Y-%m-%d").replace(day=1)
        valueDateString = valueDate.strftime("%Y-%m-%d")
        if( valueDateString not in data ):
            data[valueDateString] = {"x":valueDateString, "y":None}
        if( data[valueDateString]["y"] == None ):
            data[valueDateString]["y"] = 0
        data[valueDateString]["y"] += 1
    for _, value in data.items():
        serie["data"].append( value )
    serie["data"].sort(key=lambda x: x["x"])
    return [serie]