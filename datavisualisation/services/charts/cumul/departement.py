

#### IMPORT #################
from datetime import datetime




#### METHODS ################
def cumulMensuel( csvFile ):
    """
        Calcul le cumul mensuel pour chaque départements sur l'ensemble du jeu de données.
        :param csvFile: le jeu de données
        :type csvFile: dict
        :return: les séries de l'année en cours et de la précédente
        :rtype: dict
    """
    output = {
        "data": {
            "series": dict(),
            "codes": list(),
            "previous": None
        },
        "invalide": None
    }
    nbOperationsNonUtilisable = 0
    series = dict()
    for value in csvFile.values():
        try:
            valueDate = datetime.strptime(value["date"], "%Y-%m-%d")
            valueMonth = valueDate.month - 1
        except ValueError as error:
            print( error )
            nbOperationsNonUtilisable += 1
            continue
        else:
                try:
                    valueCode = value["code_departement"]
                    valueNom = value["nom_departement"]
                    if( valueCode == "" ):
                        nbOperationsNonUtilisable += 1
                        continue
                    if( len(valueCode) == 1 ):
                        valueCode = "0" + valueCode
                except ValueError as error:
                    print( error )
                    nbOperationsNonUtilisable += 1
                    continue
                else:
                    if( valueCode not in series ):
                        series[valueCode] = {
                            "name": valueNom,
                            "data": [None] * 12
                        }
                    if( series[valueCode]["data"][valueMonth] == None ):
                        series[valueCode]["data"][valueMonth] = 0
                    series[valueCode]["data"][valueMonth] += 1
    output["data"]["series"] = series
    output["data"]["codes"] = list( series )
    output["data"]["previous"] = output["data"]["codes"][0]
    output["invalide"] = nbOperationsNonUtilisable
    return output