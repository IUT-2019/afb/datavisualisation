

#### IMPORT #################
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired, Length
from hashlib import sha256
from ..models.Administrateur import getAdministrateurByName




#### CLASS ##################
class Login( FlaskForm ):
    username = StringField(
        "Username",
        validators = [
            DataRequired(message="Veuillez saisir un identifiant."),
            Length(min=8, max=50, message="La taille de l'identifiant n'est pas valide")
        ]
    )
    password = PasswordField(
        "Password",
        validators = [
            DataRequired(message="Veuillez saisir un mot de passe."),
            Length(min=8, max=20, message="La taille du mot de passe n'est pas valide")
        ]
    )


    def isAuthentified( self ):
        """
            Compare les informations du formulaire avec la BDD.
            :param self: Un formulaire
            :type self: Object
            :return: Un Administrateur
            :rtype: Object
        """
        user = getAdministrateurByName( self.username.data )
        passwd = sha256( self.password.data.encode() ).hexdigest()
        if( user != None and passwd == user.password ):
            return user
        return None
   



    #### OBJECT METHODS
    def __repr__( self ):
        """
            Représente l'objet dans un format compréhensible pour les affichages.
            :param self: Un formulaire
            :type self: Object
            :return: Une représentation de l'objet comportant le nom de l'admin.
            :rtype: String
        """
        return "<Login %s>" % (self.username.data)