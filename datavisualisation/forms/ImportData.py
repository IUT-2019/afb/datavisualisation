

#### IMPORT #################
from ..app import app
from ..models import Data
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Length, Regexp
from flask_wtf.file import FileField, FileRequired, FileAllowed
from re import match




#### CLASS ##################
class ImportData( FlaskForm ):
    titre = StringField(
        "titre",
        validators = [
            DataRequired(message="Veuillez saisir le titre du jeu de données"),
            Length(min=3, max=250, message="Le titre doit contenir entre 3 et 250 caractères")
        ]
    )
    nom = StringField(
        "nom",
        validators = [
            DataRequired(message="Veuillez saisir le nom du fichier"),
            Length(min=3, max=250, message="Le nom doit contenir entre 3 et 250 caractères"),
            Regexp(regex="[a-z]{3,246}.csv", message="Syntaxe invalide")
        ]
    )
    description = TextAreaField(
        "description",
        default = None,
        validators = [
            Length(max=250, message="La description ne doit pas exéder 250 caractères")
        ]
    )
    package = FileField(
        default = None,
        validators = [ 
            FileRequired(message="Vous devez choisir une fichier"),
            FileAllowed(upload_set={"jar"}, message="Le fichier doit être au format .jar")
        ]
    )
    csvFile = FileField(
        default = None,
        validators = [ 
            FileRequired(message="Vous devez choisir une fichier"),
            FileAllowed(upload_set={"csv"}, message="Le fichier doit être au format .csv")
        ]
    )



    #### OBJECT METHODS
    def __repr__( self ):
        """
            Représente l'objet dans un format compréhensible pour les affichages.
            :param self: Un formulaire
            :type self: Dict
            :return: Une représentation de l'objet comportant le nom du fichier.
            :rtype: String
        """
        return "<ImportData %s>" % (self.nom.data)
    



    def isValide( self ):
        """
            Vérifie que le formulaire est correctement remplie.
            :param self: Un formulaire
            :type self: Dict
            :return: L'état devalidité et la liste des messages d'erreurs
            :rtype: Dict
        """
        response = {"valide": True, "error": list()}
        titreSize = len( self.titre.data )
        nomSize = len( self.nom.data )
        descriptionSize = len( self.description.data )
        
        # Vérification de la validité du titre
        if( titreSize == 0 ):
            response["valide"] = False
            response["error"].append( self.titre.validators[0].message )
        elif( titreSize < self.titre.validators[1].min or titreSize > self.titre.validators[1].max ):
            response["valide"] = False
            response["error"].append( self.titre.validators[1].message )
        
        # Vérification de la validité du nom de fichier
        if( nomSize == 0 ):
            response["valide"] = False
            response["error"].append( self.nom.validators[0].message )
        elif( nomSize < self.nom.validators[1].min or nomSize > self.nom.validators[1].max ):
            response["valide"] = False
            response["error"].append( self.nom.validators[1].message )
        elif( not match(pattern=self.nom.validators[2].regex, string=self.nom.data) ):
            response["valide"] = False
            response["error"].append( self.nom.validators[2].message )
        
        # Vérification de la validité de la description
        if( descriptionSize > self.description.validators[0].max ):
            response["valide"] = False
            response["error"].append( self.description.validators[0].message )
        
        # Vérification de la validité du fichier .csv
        if( self.csvFile.data == None ):
            response["valide"] = False
            response["error"].append( self.csvFile.validators[0].message )
        elif( self.csvFile.data.filename.split(".")[-1] not in self.csvFile.validators[1].upload_set ):
            response["valide"] = False
            response["error"].append( self.csvFile.validators[1].message )

        # Vérification de la validité du package .jar
        if( self.package.data == None ):
            response["valide"] = False
            response["error"].append( self.package.validators[0].message )
        elif( self.package.data.filename.split(".")[-1] not in self.package.validators[1].upload_set ):
            response["valide"] = False
            response["error"].append( self.package.validators[1].message )
        
        return response