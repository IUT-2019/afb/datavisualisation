

#### IMPORT #################
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_breadcrumbs import Breadcrumbs
import os.path




#### METHODS ################
def mkpath(p):
    """
        Prend un chemin absolut et retourne sont chemin relatif.
        :param p: Un chemin relatif
        :type p: String
        :return: Le chemin absolut du fichier
        :rtype: String
    """
    return os.path.normpath( os.path.join( os.path.dirname(__file__), p ))




#### INIT ###################
app = Flask(__name__)




#### LOGIN ##################
login_manager = LoginManager()
login_manager.init_app(app)




#### BREADCRUMBS ############
Breadcrumbs(app=app)




#### DATABASE ###############
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["SQLALCHEMY_DATABASE_URI"] = ("sqlite:///" + mkpath("../app.db"))
app.config["SECRET_KEY"] = "d9ee54d5-f044-49da-9489-c976a146c-afb-2019"
db = SQLAlchemy( app )




#### VARIABLES ##############
#### DIRECTORY
app.config["DIRECTORY_GEOJSON"] = "datavisualisation/data/geojson/"
app.config["DIRECTORY_CSV"] = "datavisualisation/data/csv/"
app.config["DIRECTORY_DECOUPAGE"] = "datavisualisation/data/decoupage/"
app.config["DIRECTORY_TEMPORAIRE"] = "datavisualisation/temp/"
app.config["DIRECTORY_PACKAGES"] = "datavisualisation/packages/"

#### INFOS
app.config["TITLE"] = "BIOD"
app.config["DESCRIPTION"] = "Biodiversité Datavisualisation"

#### PROGRAMMES
app.config["JAVA"] = "dependences/jdk-11.0.3.7-hotspot/bin/java.exe"