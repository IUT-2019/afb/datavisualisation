

#### IMPORT #################
from flask import request, url_for
from .models.Data import getData




#### SAISIES #################
def breadcrumbHisoriqueComparatif(*args, **kwargs):
	return [
		{ 'text': "Historique comparatif" }
	]




#### SAISIES #################
def breadcrumbStatistique(*args, **kwargs):
	id = request.view_args['id']
	data = getData( id )
	return [
		{ 'text': data.titre, "url": url_for("visualisationStatistique", id=id) },
		{ 'text': "Statistique" }
	]

def breadcrumbChronologie(*args, **kwargs):
	id = request.view_args['id']
	data = getData( id )
	return [
		{ 'text': data.titre, "url": url_for("visualisationStatistique", id=id) },
		{ 'text': "Opérations par chronologie" }
	]

def breadcrumbGeographieRegion(*args, **kwargs):
	id = request.view_args['id']
	data = getData( id )
	return [
		{ 'text': data.titre, "url": url_for("visualisationStatistique", id=id) },
		{ 'text': "Opérations par région" }
	]

def breadcrumbGeographieDepartement(*args, **kwargs):
	id = request.view_args['id']
	data = getData( id )
	return [
		{ 'text': data.titre, "url": url_for("visualisationStatistique", id=id) },
		{ 'text': "Opérations par département" }
	]




#### ESPECES #################
def breadcrumbEspeceChronologie(*args, **kwargs):
	id = request.view_args['id']
	data = getData( id )
	return [
		{ 'text': data.titre, "url": url_for("visualisationStatistique", id=id) },
		{ 'text': "Groupes d'espèces par chronologie" }
	]