"use strict"

export async function SelecteurParAnnees(firstSelector, secondSelector, functions){


    // ========== EVENTS
    firstSelector.addEventListener("change", event => {
        if(event.explicitOriginalTarget.value > secondSelector.value){
            let cpt = 0;
            let trouve = false;
            while(!trouve && cpt<secondSelector.children.length){
                let option = secondSelector.children[cpt];
                if(option.value == event.explicitOriginalTarget.value){
                    option.selected = true;
                    trouve = true;
                }
                cpt += 1;
            }
        }
        else{
            for(let option of secondSelector.children){
                if(option.value < event.explicitOriginalTarget.value){
                    option.hidden = true;
                }
                else{
                    option.hidden = false;
                }
            }
        }
        for(let elem of functions){
            setTimeout( elem.nom, 0, ...elem.args, firstSelector.value, secondSelector.value );
        }
    });


    secondSelector.addEventListener("change", event => {
        for(let elem of functions){
            setTimeout( elem.nom, 0, ...elem.args, firstSelector.value, secondSelector.value );
        }
    });
}