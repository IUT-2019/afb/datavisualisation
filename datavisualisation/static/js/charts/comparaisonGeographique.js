"use strict"


// ======================================== IMPORTS
import { parameters } from "./parameters.js"




// ======================================== VARIABLES
let chartData = {"region": null, "departement": null};




// ======================================== METHODS
// ==================== Chart
/**
 * Affiche un graphe de type "Area" qui pour chaque zone géographique représente
 * les données de l'année en cours et l'année antérieur afin de réaliser une
 * comparaison de positionnement pour chaque mois de l'année.
 * @param {HTMLDivElement} target La balise cible qui accueil le graphe.
 * @param {string} key Le clée d'accès aux données.
 */
function buildChartComparaisonGeographique( target, key ){
    const options = {
        chart: {
            id: "chart-comparaison-geographie-" + key,
            height: 300,
            type: "area",
            foreColor: parameters["foreColor"],
            toolbar: {
                autoSelected: 'pan',
                show: false
            }
        },
        markers: {
            size: 4,
            colors: parameters["dataColor"],
            strokeWidth: 0,
            hover: {
                size: 6
            }
        },
        colors: parameters["dataColor"],
        stroke: {
            width: 2,
            curve: "straight"
        },
        dataLabels: {
            enabled: false
        },
        fill: {
            gradient: {
                shade: "dark"
            }
        },
        grid: {
            show: true,
            borderColor: parameters["gridBorderColor"],
            xaxis: {
                lines: {
                    show: true
                }
            },
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        tooltip: {
            followCursor: false,
            theme: parameters["tooltipTheme"],
            x: {
              show: false
            },
            marker: {
              show: true
            }
        },
        series: Object.values( chartData[key]["data"]["series"][chartData[key]["data"]["previous"]] ),
        legend: {
            show: true,
            showForSingleSeries: true,
            position: 'top',
            horizontalAlign: "left",
            fontSize: parameters["legendFontSize"]
        },
        xaxis: {
            type: 'text',
            categories: parameters["listMonth"]
        },
        yaxis: {
            min: 0
        },
        noData: {
            text: "Pas de données disponibles !",
            style: {
                color: parameters["redColor"][0],
                fontSize: parameters["legendFontSize"]
            }
        }
    };
    const chart = new ApexCharts( target, options );
    chart.render();
}




// ==================== Build
/**
 * Construit un graphe pour les départements.
 * @param {HTMLDivElement} target La balise cible qui accueil le graphe.
 * @param {Object} result Les données pour chaque zone géographique.
 */
export function buildChartComparaisonGeographiqueDepartement( target, result ){
    chartData["departement"] = result;
    buildChartComparaisonGeographique( target, "departement" )
}


/**
 * Construit un graphe pour les régions.
 * @param {HTMLDivElement} target La balise cible qui accueil le graphe.
 * @param {Object} result Les données pour chaque zone géographique.
 */
export function buildChartComparaisonGeographiqueRegion( target, result ){
    chartData["region"] = result;
    buildChartComparaisonGeographique( target, "region" )
}




// ==================== Update
/**
 * Met à jour le graphe en fonction du code de la zone géographique.
 * @param {string} key Le clée d'accès aux données.
 * @param {number} code Le code de la zone géographique.
 */
function updateChartComparaisonGeographique( key, code ){
    chartData[key]["data"]["previous"] = code;
    const series = chartData[key]["data"]["series"][code] != undefined ? Object.values(chartData[key]["data"]["series"][code]) : new Array();
    ApexCharts.exec( "chart-comparaison-geographie-" + key, "updateSeries", series );
}


/**
 * Récupère le département qui étais sélectionné et lui retire la classe selected.
 * Il affecte par la suite la classe "selected" au département que l'utilisateur
 * vient de sélectionner.
 * @param {number} code Le code du département. 
 */
export function updateChartComparaisonDepartement( code ){
    let previous = document.querySelector("#carte-france-departement-selecteur").contentDocument.documentElement.querySelector("#carte-dep-" + chartData["departement"]["data"]["previous"]);
    previous.classList.remove("selected");
    event.originalTarget.classList.add("selected");
    updateChartComparaisonGeographique( "departement", code );
}


/**
 * Récupère la région qui étais sélectionné et lui retire la classe selected.
 * Il affecte par la suite la classe "selected" à la région que l'utilisateur
 * vient de sélectionner.
 * @param {number} code Le code de la région. 
 */
export function updateChartComparaisonRegion( code ){
    let previous = document.querySelector("#carte-france-region-selecteur").contentDocument.documentElement.querySelector("#carte-reg-" + chartData["region"]["data"]["previous"]);
    previous.classList.remove("selected");
    event.originalTarget.classList.add("selected");
    updateChartComparaisonGeographique( "region", code );
}