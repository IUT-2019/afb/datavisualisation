"use strict"


// ======================================== IMPORTS
import { parameters } from "./parameters.js"




// ======================================== METHODS
// ==================== Chart
/**
 * Affiche un histogramme de l'ensemble du jeu de données avec une granulation mensuelle.
 * Le graphe est initialisé avec une vue sur les 10 dernières années.
 * @param {HTMLDivElement} grapheTarget La balise cible qui accueil le graphe.
 * @param {HTMLDivElement} selectorTarget La balise cible qui accueil la sélection temporel du graphe.
 * @param {Object} series Les données mensuelles étalé sur l'ensemble du jeu de données.
 */
export function buildChartHistorique( grapheTarget, selectorTarget, series ){
    // Le graphe
    const chartOptions = {
        chart: {
            id: "chartHistorique",
            height: 300,
            type: "bar",
            foreColor: parameters["foreColor"],
            toolbar: {
                autoSelected: 'pan',
                show: false
            }
        },
        colors: parameters["purpleColor"],
        dataLabels: {
            enabled: false
        },
        grid: {
            show: true,
            borderColor: parameters["gridBorderColor"],
            xaxis: {
                lines: {
                    show: true
                }
            },
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        tooltip: {
            followCursor: false,
            theme: parameters["tooltipTheme"],
            x: {
              show: false
            },
            marker: {
              show: false
            }
        },
        series: series,
        legend: {
            show: true,
            showForSingleSeries: true,
            position: 'top',
            horizontalAlign: "left",
            fontSize: parameters["legendFontSize"]
        },
        xaxis: {
            type: 'datetime',
            labels: {
                datetimeFormatter: {
                    year: "yyyy",
                    month: "MMM",
                    day: ""
                },
            },
            tooltip: {
                enabled: true,
                formatter: function(val, opts){
                    let date = new Date( val );
                    let result = parameters["listMonth"][date.getMonth()] + " " + date.getFullYear();
                    return result;
                }
            }
        }
    };
    const chart = new ApexCharts( grapheTarget, chartOptions );
    chart.render();




    // Le sélecteur temporel
    const vue = initVue( series );
    const selectorOptions = {
        chart: {
            height: 130,
            type: 'area',
            foreColor: parameters["foreColor"],
            brush:{
                target: "chartHistorique",
                enabled: true
            },
            selection: {
                enabled: true,
                xaxis: {
                    min: vue["min"],
                    max: vue["max"]
                }
            }
        },
        stroke: {
            width: 2,
        },
        colors: parameters["whiteColor"],
        fill: {
            gradient: {
                shade: 'dark'
            }
        },
        grid: {
            show: false,
            borderColor: parameters["gridBorderColor"],
            xaxis: {
                lines: {
                    show: true
                }
            },
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        series: series,
        xaxis: {
            type: 'datetime',
            tooltip: {
                enabled: false
            }
        }   
    };
    const chartSelector = new ApexCharts( selectorTarget, selectorOptions );
    chartSelector.render();
}




// ==================== Calculs
/**
 * Détermine la plage de temps à afficher lors de l'initialisation du graphe.
 * @param {Object} series Les données mensuelles étalé sur l'ensemble du jeu de données.
 */
function initVue( serie ){
    const size = serie[0]["data"].length;
    let duree = 10 * 12; // 10 * 12 mois = 10 ans
    if(duree >= size){ duree = size - 1; }
    const dateMax = new Date( serie[0]["data"][size - 1]["x"] ).getTime();
    const dateMin = new Date( serie[0]["data"][size - (1 + duree)]["x"] ).getTime();
    return {"max":dateMax, "min":dateMin}
}