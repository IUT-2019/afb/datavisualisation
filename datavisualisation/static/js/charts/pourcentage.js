"use strict"


// ======================================== IMPORTS
import { directionsRegionales as referencesDirectionsRegionales } from "../utils/references/directionsRegionales.js"
import { departements as referencesDepartements } from "../utils/references/departements.js"
import { parameters } from "./parameters.js"




// ======================================== VARIABLES
let chart;
let data;




// ======================================== METHODS
// ==================== Chart
/**
 * Construit et affiche un diagramme circulaire représentant le nombre de
 * saisies (occurences dans le jeu de données) par direction régionales.
 * @param {Object} csvFile le jeu de donées
 * @param {number} startyear la date de début
 * @param {number} endyear la date de fin
 */
export async function buildChartPourcentage( target, csvFile, startyear, endyear ){
    data = csvFile;
    const chartData = await calculateDirectionRegional( startyear, endyear );
    const options = {
        chart: {
            width: '100%',
            type: 'donut',
            foreColor: parameters["foreColor"]
        },
        dataLabels: {
            enabled: false
        },
        plotOptions: {
            pie: {
                size: 100,
                expandOnClick: false,
                donut: {
                    size: '90%',
                    labels: {
                        show: true,
                        name: {
                            show: false,
                        },
                        chartData: {
                            show: true,
                            fontSize: "40px",
                            offsetY: 10,
                            formatter: function (val) {
                                return Math.trunc( val * 100 / chartData["total"] ) + "%"
                            }
                        },
                        total: {
                            show: true,
                        }
                    }
                }
            }
          },
        colors: parameters["directionRegionalColors"],
        stroke: {
            width: 0
        },
        tooltip: {
            theme: parameters["tooltipTheme"]
        },
        series: chartData["series"],
        labels: chartData["labels"],
        legend: {
            show: true,
            showForSingleSeries: true,
            position: 'bottom',
            horizontalAlign: "left",
        }
    };
    chart = new ApexCharts( target, options );
    chart.render();
}




// ==================== Update
/**
 * Met à jour le graphe en fonction d'une date de début et de fin.
 * @param {number} startyear la date de début
 * @param {number} endyear la date de fin
 */
export async function updateChartPourcentage( startyear, endyear ){
    const chartData = await calculateDirectionRegional( startyear, endyear );
    chart.updateSeries( chartData["series"] );
    const options = {
        labels: chartData["labels"],
        plotOptions: {
            pie: {
                donut: {
                    labels: {
                        chartData: {
                            formatter: function(val){ 
                                let pourcent = val * 100 / chartData["total"];
                                return pourcent.toFixed(2) + "%";
                            }
                        }
                    }
                }
            }
        }
    };
    chart.updateOptions( options );
}




// ==================== Calculs
/**
 * Calcul la représentation de chaque direction régional pour une tranche
 * d'année donnée.
 * @param {number} startyear la date de début
 * @param {number} endyear la date de fin
 */
function calculateDirectionRegional( startyear, endyear ){
    return new Promise( resolve => {
        let series = new Object();
        let chartData = new Object();
        let total = 0;
        for(let id in data){
            const year = new Date( data[id]["date"] ).getFullYear().toString();
            if( startyear <= year && year <= endyear ){
                const codeDepartement = data[id]["code_departement"];
                try{
                    const codeDirectionRegional = referencesDepartements[codeDepartement]["directionRegional"];
                    const nomDirectionRegional = referencesDirectionsRegionales[codeDirectionRegional]["nom"];
                    if( !(nomDirectionRegional in series) ){
                        series[nomDirectionRegional] = 1;
                    }
                    else{
                        series[nomDirectionRegional] += 1;
                    }
                    total += 1;
                }
                catch (error){
                    continue;
                }
            }
        }
        chartData["labels"] = Object.keys( series );
        chartData["series"] = Object.values( series );
        chartData["total"] = total;
        resolve( chartData );
    });
}