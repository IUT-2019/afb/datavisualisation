"use strict"


// ======================================== IMPORTS
import { parameters } from "./parameters.js"




// ======================================== METHODS
// ==================== Chart
/**
 * Construit un graphe linéaire représentant l'évolution du nombre de saisies
 * de la dernière année par rapport à l'année précédente.
 * @param {HTMLDivElement} target La balise cible qui accueil le graphe.
 * @param {Object} series Les données pour l'année en cours et l'année antérieur.
 */
export function buildChartComparaisonChronologique( target, series ){
    const options = {
        chart: {
            height: parameters["height"],
            type: "line",
            foreColor: parameters["foreColor"],
            toolbar: {
                autoSelected: 'pan',
                show: false
            }
        },
        markers: {
            size: 4,
            colors: parameters["markersColor"],
            strokeWidth: 2,
            strokeColor: parameters["dataColor"],
            hover: {
                size: 6
            }
        },
        colors: parameters["dataColor"],
        stroke: {
            width: 2,
        },
        grid: {
            show: true,
            borderColor: parameters["gridBorderColor"],
            xaxis: {
                lines: {
                    show: true
                }
            },
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        tooltip: {
            followCursor: false,
            theme: parameters["tooltipTheme"],
            x: {
              show: false
            },
            marker: {
              show: true
            }
        },
        series: series["data"],
        legend: {
            show: true,
            showForSingleSeries: true,
            position: 'top',
            horizontalAlign: "left",
            fontSize: parameters["legendFontSize"]
        },
        xaxis: {
            type: 'text',
            categories: parameters["listMonth"]
        }
    };
    const chart = new ApexCharts( target, options );
    chart.render();

    showErrors(series["invalide"], target);
}


function showErrors( error, chart ){
    if( error != null ){
        let target = chart.parentElement.parentElement.children[2];
        target.textContent = error + "% des données sont non représentable !";
    }
}