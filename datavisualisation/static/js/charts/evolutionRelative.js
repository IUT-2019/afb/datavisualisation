"use strict"


// ======================================== IMPORTS
import { parameters } from "./parameters.js"




// ======================================== METHODS
// ==================== Chart
/**
 * Construit un graphe linéaire représentant l'évolution du nombre d'opérations
 * réalisé dans les différent jeux de données durant les 10 dernières années.
 * @param {HTMLDivElement} target La balise cible qui accueil le graphe.
 * @param {Object} series Les données de chaques jeux de données dans l'application.
 */
export function buildChartEvolutionRelative( target, series ){
    const options = {
        chart: {
            height: parameters["height"],
            type: "line",
            foreColor: parameters["foreColor"],
            toolbar: {
                autoSelected: 'pan',
                show: false
            }
        },
        markers: {
            size: 4,
            colors: parameters["markersColor"],
            strokeWidth: 2,
            strokeColor: parameters["dataColor"],
            hover: {
                size: 6,
                colors: parameters["dataColor"],
            }
        },
        colors: parameters["dataColor"],
        stroke: {
            width: 2,
        },
        grid: {
            show: true,
            borderColor: parameters["gridBorderColor"],
            xaxis: {
                lines: {
                    show: true
                }
            },
            yaxis: {
                lines: {
                    show: true
                }
            }
        },
        tooltip: {
            followCursor: false,
            theme: parameters["tooltipTheme"],
            x: {
              show: false
            },
            marker: {
              show: true
            }
        },
        series: series,
        legend: {
            show: true,
            showForSingleSeries: true,
            position: 'top',
            horizontalAlign: "left",
            fontSize: parameters["legendFontSize"]
        }
    };
    const chart = new ApexCharts( target, options );
    chart.render();
}