"use strict"


// ======================================== VARIABLES
export let parameters = {
    "height": 400,
    "listMonth": ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
    "foreColor": "#98a6ad", // La couleur du texte (gris claire)
    "dataColor": ["#77c476", "#fcc163", "#f27faa", "#f16870", "#9e6bae", "#1bb2cf"], // Les couleur de données (vert, jaune, rose, rouge, violet, bleu)
    "greenColor": ["#77c476"], // vert
    "yellowColor": ["#fcc163"], // jaune
    "pinkColor": ["#f27faa"], // rose
    "redColor": ["#f16870"], // rouge
    "purpleColor": ["#9e6bae"], // violet
    "blueColor": ["#1bb2cf"], // bleu
    "whiteColor": ["#ffffff"], // blanc
    "directionRegionalColors": ["#77c476", "#9e6bae", "#f0679f", "#95cd76", "#5a86c5", "#7963ab", "#36bc99", "#be7db7", "#38b5e5", "#4cb6b6", "#fcc163"],
    "fillShade": "dark", // thème de dégradé pour les graphes de type 'area'
    "gridBorderColor": "#3d4853", // La couleur de bordure du cadrillage (gris)
    "tooltipTheme": "dark", // Le thème des tooltips
    "markersColor": "#36404a", // gris
    "legendFontSize": "15px"
}