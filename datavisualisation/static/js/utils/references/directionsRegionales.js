"use strict"


export const directionsRegionales = {
    "01": {
        "code": "01",
        "nom": "NORMANDIE-HAUTS DE FRANCE",
        "departements": ["62", "59", "80", "60", "02", "50", "14", "61", "27", "76"]
    },
    "02": {
        "code": "02",
        "nom": "BRETAGNE-PAYS DE LA LOIRE",
        "departements": ["29", "22", "56", "35", "53", "72", "49", "44", "85"]
    },
    "03": {
        "code": "03",
        "nom": "GRAND EST",
        "departements": ["08", "51", "10", "52", "55", "54", "57", "88", "68", "67"]
    },
    "04": {
        "code": "04",
        "nom": "CENTRE-VAL DE LOIRE",
        "departements": ["28", "45", "41", "37", "36", "18"]
    },
    "05": {
        "code": "05",
        "nom": "AUVERGNE-RHONE-ALPES",
        "departements": ["03", "63", "15", "43", "42", "69", "01", "38", "26", "07", "73", "74"]
    },
    "06": {
        "code": "06",
        "nom": "NOUVELLE-AQUITAINE",
        "departements": ["79", "86", "17", "16", "87", "23", "19", "24", "33", "47", "40", "64"]
    },
    "07": {
        "code": "07",
        "nom": "OCCITANIE",
        "departements": ["46", "82", "32", "65", "48", "12", "81", "31", "09", "66", "11", "34", "30"]
    },
    "08": {
        "code": "08",
        "nom": "PROVENCE-ALPES-COTE D'AZURE-CORSE",
        "departements": ["05", "04", "06", "83", "84", "13", "2A", "2B"]
    },
    "09": {
        "code": "09",
        "nom": "BOURGOGNE-FRANCHE-COMTE",
        "departements": ["89", "58", "21", "71", "70", "25", "39", "90"]
    },
    "10": {
        "code": "10",
        "nom": "ILE-DE-FRANCE",
        "departements": ["95", "77", "78", "91", "93", "92", "94", "75"]
    },
    "11": {
        "code": "11",
        "nom": "OUTRE-MER",
        "departements": ["971", "972", "973", "974", "976"]
    }
};