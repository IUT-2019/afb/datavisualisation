"use strict"


export const regions = {
    "32": {
        "code": "32",
        "nom": "HAUTS-DE-FRANCE",
        "departements": ["62", "59", "80", "60", "02"]
    },
    "44": {
        "code": "44",
        "nom": "GRAND EST",
        "departements": ["08", "51", "10", "52", "55", "54", "57", "88", "68", "67"]
    },
    "27": {
        "code": "27",
        "nom": "BOURGOGNE-FRANCHE-COMTE",
        "departements": ["89", "58", "21", "71", "70", "25", "39", "90"]
    },
    "11": {
        "code": "11",
        "nom": "ILE-DE-FRANCE",
        "departements": ["95", "77", "78", "91", "93", "92", "94", "75"]
    },
    "28": {
        "code": "28",
        "nom": "NORMANDIE",
        "departements": ["50", "14", "61", "27", "76"]
    },
    "53": {
        "code": "53",
        "nom": "BRETAGNE",
        "departements": ["29", "22", "56", "35"]
    },
    "52": {
        "code": "52",
        "nom": "PAYS DE LA LOIRE",
        "departements": ["53", "72", "49", "44", "85"]
    },
    "24": {
        "code": "24",
        "nom": "CENTRE-VAL DE LOIRE",
        "departements": ["28", "45", "41", "37", "36", "18"]
    },
    "75": {
        "code": "75",
        "nom": "NOUVELLE-AQUITAINE",
        "departements": ["79", "86", "17", "16", "87", "23", "19", "24", "33", "47", "40", "64"]
    },
    "84": {
        "code": "84",
        "nom": "AUVERGNE-RHONE-ALPES",
        "departements": ["03", "63", "15", "43", "42", "69", "01", "38", "26", "07", "73", "74"]
    },
    "76": {
        "code": "76",
        "nom": "OCCITANIE",
        "departements": ["46", "82", "32", "65", "48", "12", "81", "31", "09", "66", "11", "34", "30"]
    },
    "93": {
        "code": "93",
        "nom": "PROVENCE-ALPES-COTE D'AZURE",
        "departements": ["05", "04", "06", "83", "84", "13"]
    },
    "94": {
        "code": "94",
        "nom": "CORSE",
        "departements": ["2A", "2B"]
    }
};