"use strict"


export const departements = {
    "62": {
        "code": "62",
        "nom": "PAS-DE-CALAIS",
        "region": "32",
        "directionRegional": "01"
    },
    "59": {
        "code": "59",
        "nom": "NORD",
        "region": "32",
        "directionRegional": "01"
    },
    "80": {
        "code": "80",
        "nom": "SOMME",
        "region": "32",
        "directionRegional": "01"
    },
    "02": {
        "code": "02",
        "nom": "AISNE",
        "region": "32",
        "directionRegional": "01"
    },
    "60": {
        "code": "60",
        "nom": "OISE",
        "region": "32",
        "directionRegional": "01"
    },
    "08": {
        "code": "08",
        "nom": "ARDENNES",
        "region": "44",
        "directionRegional": "03"
    },
    "51": {
        "code": "51",
        "nom": "MARNE",
        "region": "44",
        "directionRegional": "03"
    },
    "10": {
        "code": "10",
        "nom": "AUBE",
        "region": "44",
        "directionRegional": "03"
    },
    "55": {
        "code": "55",
        "nom": "MEUSE",
        "region": "44",
        "directionRegional": "03"
    },
    "52": {
        "code": "52",
        "nom": "HAUTE-MARNE",
        "region": "44",
        "directionRegional": "03"
    },
    "54": {
        "code": "54",
        "nom": "MEURTHE-ET-MOSELLE",
        "region": "44",
        "directionRegional": "03"
    },
    "88": {
        "code": "88",
        "nom": "VOSGES",
        "region": "44",
        "directionRegional": "03"
    },
    "67": {
        "code": "67",
        "nom": "BAS-RHIN",
        "region": "44",
        "directionRegional": "03"
    },
    "68": {
        "code": "68",
        "nom": "HAUT-RHIN",
        "region": "44",
        "directionRegional": "03"
    },
    "57": {
        "code": "57",
        "nom": "MOSELLE",
        "region": "44",
        "directionRegional": "03"
    },
    "89": {
        "code": "89",
        "nom": "YONNE",
        "region": "27",
        "directionRegional": "09"
    },
    "58": {
        "code": "58",
        "nom": "NIEVRE",
        "region": "27",
        "directionRegional": "09"
    },
    "21": {
        "code": "21",
        "nom": "COTE-D'OR",
        "region": "27",
        "directionRegional": "09"
    },
    "71": {
        "code": "71",
        "nom": "SAONE-ET-LOIRE",
        "region": "27",
        "directionRegional": "09"
    },
    "70": {
        "code": "70",
        "nom": "HAUTE-SAONE",
        "region": "27",
        "directionRegional": "09"
    },
    "90": {
        "code": "90",
        "nom": "TERRITOIRE-DE-BELFORT",
        "region": "27",
        "directionRegional": "09"
    },
    "25": {
        "code": "25",
        "nom": "DOUBS",
        "region": "27",
        "directionRegional": "09"
    },
    "39": {
        "code": "39",
        "nom": "JURA",
        "region": "27",
        "directionRegional": "09"
    },
    "95": {
        "code": "95",
        "nom": "VAL D'OISE",
        "region": "11",
        "directionRegional": "10"
    },
    "78": {
        "code": "78",
        "nom": "YVELINES",
        "region": "11",
        "directionRegional": "10"
    },
    "91": {
        "code": "91",
        "nom": "ESSONE",
        "region": "11",
        "directionRegional": "10"
    },
    "77": {
        "code": "77",
        "nom": "SEINE ET MARNE",
        "region": "11",
        "directionRegional": "10"
    },
    "93": {
        "code": "93",
        "nom": "SEINE SAINT DENIS",
        "region": "11",
        "directionRegional": "10"
    },
    "92": {
        "code": "92",
        "nom": "HAUTS DE SEINE",
        "region": "11",
        "directionRegional": "10"
    },
    "94": {
        "code": "94",
        "nom": "VAL DE MARNE",
        "region": "11",
        "directionRegional": "10"
    },
    "75": {
        "code": "75",
        "nom": "PARIS",
        "region": "11",
        "directionRegional": "10"
    },
    "50": {
        "code": "50",
        "nom": "MANCHE",
        "region": "28",
        "directionRegional": "01"
    },
    "14": {
        "code": "14",
        "nom": "CALVADOS",
        "region": "28",
        "directionRegional": "01"
    },
    "76": {
        "code": "76",
        "nom": "SEINE MARITIME",
        "region": "28",
        "directionRegional": "01"
    },
    "27": {
        "code": "27",
        "nom": "EURE",
        "region": "28",
        "directionRegional": "01"
    },
    "61": {
        "code": "61",
        "nom": "ORNE",
        "region": "28",
        "directionRegional": "01"
    },
    "29": {
        "code": "29",
        "nom": "FINISTERE",
        "region": "53",
        "directionRegional": "02"
    },
    "22": {
        "code": "22",
        "nom": "COTES D'ARMOR",
        "region": "53",
        "directionRegional": "02"
    },
    "56": {
        "code": "56",
        "nom": "MORBIHAN",
        "region": "53",
        "directionRegional": "02"
    },
    "35": {
        "code": "35",
        "nom": "ILLE ET VILAINE",
        "region": "53",
        "directionRegional": "02"
    },
    "53": {
        "code": "53",
        "nom": "MAYENNE",
        "region": "52",
        "directionRegional": "52"
    },
    "72": {
        "code": "72",
        "nom": "SARTHE",
        "region": "52",
        "directionRegional": "52"
    },
    "44": {
        "code": "44",
        "nom": "LOIRE ATLANTIQUE",
        "region": "52",
        "directionRegional": "52"
    },
    "49": {
        "code": "49",
        "nom": "MAINE ET LOIRE",
        "region": "52",
        "directionRegional": "52"
    },
    "85": {
        "code": "85",
        "nom": "VENDEE",
        "region": "52",
        "directionRegional": "52"
    },
    "28": {
        "code": "28",
        "nom": "EURE ET LOIRE",
        "region": "24",
        "directionRegional": "04"
    },
    "45": {
        "code": "45",
        "nom": "LOIRET",
        "region": "24",
        "directionRegional": "04"
    },
    "41": {
        "code": "41",
        "nom": "LOIRE ET CHER",
        "region": "24",
        "directionRegional": "04"
    },
    "37": {
        "code": "37",
        "nom": "INDRE ET LOIRE",
        "region": "24",
        "directionRegional": "04"
    },
    "18": {
        "code": "18",
        "nom": "CHER",
        "region": "24",
        "directionRegional": "04"
    },
    "36": {
        "code": "36",
        "nom": "INDRE",
        "region": "24",
        "directionRegional": "04"
    },
    "79": {
        "code": "79",
        "nom": "DEUX SEVRES",
        "region": "75",
        "directionRegional": "06"
    },
    "86": {
        "code": "86",
        "nom": "VIENNE",
        "region": "75",
        "directionRegional": "06"
    },
    "17": {
        "code": "17",
        "nom": "CHARENTE MARITIME",
        "region": "75",
        "directionRegional": "06"
    },
    "16": {
        "code": "16",
        "nom": "CHARENTE",
        "region": "75",
        "directionRegional": "06"
    },
    "87": {
        "code": "87",
        "nom": "HAUTE VIENNE",
        "region": "75",
        "directionRegional": "06"
    },
    "23": {
        "code": "23",
        "nom": "CREUSE",
        "region": "75",
        "directionRegional": "06"
    },
    "19": {
        "code": "19",
        "nom": "CORREZE",
        "region": "75",
        "directionRegional": "06"
    },
    "24": {
        "code": "24",
        "nom": "DORDOGNE",
        "region": "75",
        "directionRegional": "06"
    },
    "33": {
        "code": "33",
        "nom": "GIRONDE",
        "region": "75",
        "directionRegional": "06"
    },
    "47": {
        "code": "47",
        "nom": "LOT ET GARONNE",
        "region": "75",
        "directionRegional": "06"
    },
    "40": {
        "code": "40",
        "nom": "LANDRES",
        "region": "75",
        "directionRegional": "06"
    },
    "64": {
        "code": "64",
        "nom": "PYRENEES ATLANTIQUES",
        "region": "75",
        "directionRegional": "06"
    },
    "03": {
        "code": "03",
        "nom": "ALLIER",
        "region": "84",
        "directionRegional": "05"
    },
    "63": {
        "code": "63",
        "nom": "PUY DE DOME",
        "region": "84",
        "directionRegional": "05"
    },
    "15": {
        "code": "15",
        "nom": "CANTAL",
        "region": "84",
        "directionRegional": "05"
    },
    "42": {
        "code": "42",
        "nom": "LOIRE",
        "region": "84",
        "directionRegional": "05"
    },
    "43": {
        "code": "43",
        "nom": "HAUTE LOIRE",
        "region": "84",
        "directionRegional": "05"
    },
    "69": {
        "code": "69",
        "nom": "RHONE",
        "region": "84",
        "directionRegional": "05"
    },
    "07": {
        "code": "07",
        "nom": "ARDECHE",
        "region": "84",
        "directionRegional": "05"
    },
    "01": {
        "code": "01",
        "nom": "AIN",
        "region": "84",
        "directionRegional": "05"
    },
    "38": {
        "code": "38",
        "nom": "ISERE",
        "region": "84",
        "directionRegional": "05"
    },
    "26": {
        "code": "26",
        "nom": "DROME",
        "region": "84",
        "directionRegional": "05"
    },
    "74": {
        "code": "74",
        "nom": "HAUTE SAVOIE",
        "region": "84",
        "directionRegional": "05"
    },
    "73": {
        "code": "73",
        "nom": "SAVOIE",
        "region": "84",
        "directionRegional": "05"
    },
    "46": {
        "code": "46",
        "nom": "LOT",
        "region": "76",
        "directionRegional": "07"
    },
    "82": {
        "code": "82",
        "nom": "TARN ET GARONNE",
        "region": "76",
        "directionRegional": "07"
    },
    "32": {
        "code": "32",
        "nom": "GERS",
        "region": "76",
        "directionRegional": "07"
    },
    "65": {
        "code": "65",
        "nom": "HAUTES PYRENEES",
        "region": "76",
        "directionRegional": "07"
    },
    "31": {
        "code": "31",
        "nom": "HAUTE GARONNE",
        "region": "76",
        "directionRegional": "07"
    },
    "81": {
        "code": "81",
        "nom": "TARN",
        "region": "76",
        "directionRegional": "07"
    },
    "12": {
        "code": "12",
        "nom": "AVEYRON",
        "region": "76",
        "directionRegional": "07"
    },
    "48": {
        "code": "48",
        "nom": "LOZERE",
        "region": "76",
        "directionRegional": "07"
    },
    "30": {
        "code": "30",
        "nom": "GARD",
        "region": "76",
        "directionRegional": "07"
    },
    "34": {
        "code": "34",
        "nom": "HERAULT",
        "region": "76",
        "directionRegional": "07"
    },
    "11": {
        "code": "11",
        "nom": "AUDE",
        "region": "76",
        "directionRegional": "07"
    },
    "66": {
        "code": "66",
        "nom": "PYRENEES ORIENTALES",
        "region": "76",
        "directionRegional": "07"
    },
    "09": {
        "code": "09",
        "nom": "ARIEGE",
        "region": "76",
        "directionRegional": "07"
    },
    "05": {
        "code": "05",
        "nom": "HAUTES ALPES",
        "region": "93",
        "directionRegional": "08"
    },
    "84": {
        "code": "84",
        "nom": "VAUCLUSE",
        "region": "93",
        "directionRegional": "08"
    },
    "13": {
        "code": "13",
        "nom": "BOUCHES DU RHONE",
        "region": "93",
        "directionRegional": "08"
    },
    "04": {
        "code": "04",
        "nom": "ALPES DE HAUTE PROVENCE",
        "region": "93",
        "directionRegional": "08"
    },
    "06": {
        "code": "06",
        "nom": "ALPES MARITIMES",
        "region": "93",
        "directionRegional": "08"
    },
    "83": {
        "code": "83",
        "nom": "VAR",
        "region": "93",
        "directionRegional": "08"
    },
    "2B": {
        "code": "2B",
        "nom": "HAUTE CORSE",
        "region": "94",
        "directionRegional": "08"
    },
    "2A": {
        "code": "2A",
        "nom": "CORSE DU SUD",
        "region": "94",
        "directionRegional": "08"
    },
    "971": {
        "code": "971",
        "nom": "GUADELOUPE",
        "region": null,
        "directionRegional": "11"
    },
    "972": {
        "code": "972",
        "nom": "MARTINIQUE",
        "region": null,
        "directionRegional": "11"
    },
    "973": {
        "code": "973",
        "nom": "GUYANE FRANCAISE",
        "region": null,
        "directionRegional": "11"
    },
    "974": {
        "code": "974",
        "nom": "LA REUNION",
        "region": null,
        "directionRegional": "11"
    },
    "976": {
        "code": "976",
        "nom": "MAYOTTE",
        "region": null,
        "directionRegional": "11"
    },
};