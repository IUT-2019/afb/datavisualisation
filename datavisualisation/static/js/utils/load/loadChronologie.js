"use strict"


// ======================================== IMPORTS
import { buildChartComparaisonChronologique } from "../../charts/comparaisonChronologique.js"
import { buildChartHistorique } from "../../charts/historique.js"
import { buildChartPourcentage, updateChartPourcentage } from "../../charts/pourcentage.js"
import { mapOperationsParAnneesParDepartement } from "../../maps/operationsParAnneesParDepartement.js"
import { SelecteurParAnnees } from "../../selectors/selecteursParAnnees.js"




 // ======================================== REQUEST
$.ajax({
    url: "data/",
    type: "GET",
    dataType: "json",
    Origin: "0.0.0.0:5000/",
    success: function( result ){

        const data = JSON.parse( result.data );

        try {
            // Graphe sur le nombre d'opération sur l'année n et n-1
            buildChartComparaisonChronologique(
                document.querySelector('#chart-comparaison-chonologique'),
                result.seriesOperationsParMois
            );
        } catch (error) {
            console.log(error);
        }
            

        try {
            // Histogramme de l'ensemble du jeu de données
            buildChartHistorique(
                document.querySelector('#chart-historique'),
                document.querySelector("#chart-historique-selecteur"),
                result.seriesOperationsHistorique
            );
        } catch (error) {
            console.log(error);
        }

            
        const firstSelector = document.querySelector("#debut-select-year-departement");
        const secondSelector = document.querySelector("#fin-select-year-departement");
        try {
            // Graphe sur la réartition des directions régionales.
            setTimeout(() => {
                buildChartPourcentage (
                    document.querySelector('#chart-direction-regional'),
                    data,
                    firstSelector.value,
                    secondSelector.value
                );
            }, 0);
        } catch (error) {
            console.log(error);
        }


        try {
            // Carte des directions régionales
            mapOperationsParAnneesParDepartement (
                data,
                firstSelector.value,
                secondSelector.value
            );
        } catch (error) {
            console.log(error);
        }


        try {
            // Sélecteur temporel relieé à une carte et un graphe
            SelecteurParAnnees (
                firstSelector,
                secondSelector,
                [
                    {
                        "nom": updateChartPourcentage,
                        "args": []
                    },
                    {
                        "nom": mapOperationsParAnneesParDepartement,
                        "args": [data]
                    }
                ]
            );
        } catch (error) {
            console.log(error);
        }

        document.querySelector("#loader-mask").remove();
    },
    error: function(error){
        document.body.innerHTML = error.responseText;
    }
});