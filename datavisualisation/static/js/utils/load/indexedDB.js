"use strict"


// On vérifie si le navigateur supporte bien l'API indexedDB
if (!window.indexedDB) { console.log("CLIENT: Le navigateur ne supporte pas l'indexedDB."); }


// ======================================== VARIABLES
let cartographieData;
let db;




// ======================================== INITIALISATION
window.onload = function(){

   // Mise à jour du numéro de version de la base de donnée. Ce numéro permet 
   // de mettre à jour la base de données à chaque rafraichissement de 
   // la page. Le numéro de version est dépend de la session donc il est 
   // éffacé à la fermeture de la page.
   let version = 1;
   if (sessionStorage.getItem("version")){ version = parseInt(sessionStorage.getItem("version")) + 1; }
   sessionStorage.setItem("version", version);


   // Récupération des données côté serveur. On réalise une requete Ajax donc
   // asynchrone pour ne pas bloquer le navigateur (= tâche de fond). Une fois
   // les données récupérées, on charge la variable "cartograpgieData" de ces
   // données.
   let req = new XMLHttpRequest();
   req.onerror = function(event){ console.log("CLIENT: Erreur de chargement (", event.srcElement.statusText, ")"); }
   req.onload = function(event){ console.log("CLIENT: En cours dechargement (", event.srcElement.statusText, ")"); }
   req.onloadend = function(event){ console.log("CLIENT: Chargement terminé (", event.srcElement.statusText, ")");
      
      cartographieData = JSON.parse(event.srcElement.responseText); // Désérialisation du JSON => transformation en objet JavaScript.
      if(version == 1){ window.indexedDB.deleteDatabase("datavisualisation"); } // Si c'est une nouvelle session, on supprime la BDD.
      let req = window.indexedDB.open("datavisualisation", version); // On se connecte à la BDD. Si elle n'existe pas, on la construit.
      
      req.onerror = function(event) { console.log("CLIENT: Connexion base de données (", event.srcElement.error, ")"); } // Connexion BDD error
      req.onsuccess = function(event) { db = req.result; console.log("CLIENT: Connexion à la base de données", event.srcElement.result.name, "( OK )"); } // Connexion BDD success
      
      // Création ou mise à jour de la table "cartographie"
      req.onupgradeneeded = function(event) {
         db = event.target.result;
         if(event.target.result.objectStoreNames.length != 0){ db.deleteObjectStore("cartographie"); } // Suppression de la table si elle existe
         let objectStore = db.createObjectStore("cartographie", {keyPath: "id", autoIncrement: true}); // Création de la table (rendus possible)
         for (let i in cartographieData){ objectStore.add(cartographieData[i]); } // Chargement des données dans la table "cartographie".
      }
   }
   req.open("POST", "/load/4/", true); // Ouverture d'une connexion avec le serveur (méthode, url, asynchrone=true/synchrone=false)
   req.send(); // Envoie de la requete vers le serveur (ici, pas de données à envoyer)
}




// ======================================== METHODS
/**
 * Récupère l'objet de la Base de données "datavisualisation" dans la table
 * "cartographie" dont l'identifiant "id" est passé en paramètre.
 * @param {int} id 
 */
function getObject( id ) {
   setTimeout(function(){
      let objectStore = db.transaction(["cartographie"]).objectStore("cartographie"); // Récupère la table "cartographie"
      let request = objectStore.get( id ); // Création d'une requète
      
      request.onerror = function(event) { console.log("CLIENT: Echec de récupération de la donnée (", event.srcElement.error, ")"); } // Si erreur d'exécution
      request.onsuccess = function(event) {
         if(request.result) { console.log(request.result); } // Si il y a un résultat
         else { console.log("CLIENT: donnée non trouvé (", event.srcElement.error, ")"); } // Si il n'y a pas de résultat
      };
   }, 0, id); // Tâche asynchrone (function=anonyme, milliseconde=0, parametre1=id)
}


/**
 * Récupère tous les objets (ligne ou occurence) de la base de
 * données.
 */
function readAll() {
   setTimeout(function(){
      let objectStore = db.transaction("cartographie").objectStore("cartographie"); // Récupère la table "cartographie"
   
      // Parcour par curseur
      objectStore.openCursor().onsuccess = function(event) {
         let cursor = event.target.result;
         
         if (cursor) {
            console.log(cursor.value);
            cursor.continue(); // Incrémentation du curseur
         }
         else {
            console.log("Fin de la table !");
         }
      };
   }, 0); // Tâche asynchrone (function=anonyme, milliseconde=0) pour ne pas bloquer le navigateur.
}