"use strict"


// ======================================== IMPORTS
import { buildChartComparaisonChronologique } from "../../../charts/comparaisonChronologique.js"




// ======================================== REQUEST
$.ajax({
    url: "data/",
    type: "GET",
    dataType: "json",
    Origin: "0.0.0.0:5000/",
    success: function( result ){

        try {
            // Graphe sur le nombre d'opération sur l'année n et n-1
            buildChartComparaisonChronologique(
                document.querySelector('#chart-comparaison-chonologique'),
                result.seriesOperationsParMois
            );
        } catch (error) {
            console.log(error);
        }

        document.querySelector("#loader-mask").remove();
    },
    error: function(error){
        document.body.innerHTML = error.responseText;
    }
});