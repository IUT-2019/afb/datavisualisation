"use strict"


 // ======================================== IMPORTS
 import { buildChartEvolutionRelative } from "../../charts/evolutionRelative.js"




 // ======================================== REQUEST
$.ajax({
    url: "data/",
    type: "GET",
    dataType: "json",
    Origin: "0.0.0.0:5000/",
    success: function( result ){

        try {
            buildChartEvolutionRelative (document.querySelector('#chart-evolution-relative-statistique'), result.seriesOperationsElementaires);
        } catch (error) {
            console.log(error);
        }

        document.querySelector("#loader-mask").remove();
    },
    error: function(error){
        document.body.innerHTML = error.responseText;
    }
});