"use strict"


/**
 * Cette section a pour objectif le chargement du jeu de données que 
 * l'utilisateur souhaite consulter. Pour ce faire, nous executons
 * une requête AJAX qui nous retourne un objet JSON.
 */




 // ======================================== IMPORTS
import { buildChartComparaisonChronologique } from "../../charts/comparaisonChronologique.js"
import { buildChartComparaisonGeographiqueDepartement, buildChartComparaisonGeographiqueRegion, updateChartComparaisonDepartement, updateChartComparaisonRegion } from "../../charts/comparaisonGeographique.js"
import { buildChartCumulMoisDepartement, buildChartCumulMoisRegion, updateChartCumulMoisDepartement, updateChartCumulMoisRegion } from "../../charts/cumul.js"
import { buildChartEvolutionRelative } from "../../charts/evolutionRelative.js"
import { buildChartHistorique } from "../../charts/historique.js"
import { buildChartPourcentage, updateChartPourcentage } from "../../charts/pourcentage.js"

 
 import { mapOperationsParAnneesParDepartement } from "../../maps/operationsParAnneesParDepartement.js"
 import { fillMapPourcent } from "../../maps/fillMap.js"
 import { initSelectorMap } from "../../maps/selectorMap.js"
 import { SelecteurParAnnees } from "../../selectors/selecteursParAnnees.js"




// ======================================== VARIABLES
// Variable de données
export let data;

// Variables d'option pour les graphes
export let parameters = {
    "height": 400,
    "listMonth": ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
    "foreColor": "#98a6ad", // La couleur du texte (gris claire)
    "dataColor": ["#77c476", "#fcc163", "#f27faa", "#f16870", "#9e6bae", "#1bb2cf"], // Les couleur de données (vert, jaune, rose, rouge, violet, bleu)
    "greenColor": ["#77c476"], // vert
    "yellowColor": ["#fcc163"], // jaune
    "pinkColor": ["#f27faa"], // rose
    "redColor": ["#f16870"], // rouge
    "purpleColor": ["#9e6bae"], // violet
    "blueColor": ["#1bb2cf"], // bleu
    "whiteColor": ["#ffffff"], // blanc
    "directionRegionalColors": ["#77c476", "#9e6bae", "#f0679f", "#95cd76", "#5a86c5", "#7963ab", "#36bc99", "#be7db7", "#38b5e5", "#4cb6b6", "#fcc163"],
    "fillShade": "dark", // thème de dégradé pour les graphes de type 'area'
    "gridBorderColor": "#3d4853", // La couleur de bordure du cadrillage (gris)
    "tooltipTheme": "dark", // Le thème des tooltips
    "markersColor": "#36404a", // gris
    "legendFontSize": "15px"
}




// ======================================== REQUEST
$.ajax({
    url: "data/",
    type: "GET",
    dataType: "json",
    Origin: "0.0.0.0:5000/",
    success: function( result ){

        data = JSON.parse( result.data );
        

        // ==================== Statistique
        if(document.querySelector('#chart-evolution-relative-statistique') != null) {
            buildChartEvolutionRelative (
                document.querySelector('#chart-evolution-relative-statistique'),
                result.seriesOperationsElementaires,
                parameters
            );
        }




        // ==================== Chronologie
        if(document.querySelector('#chart-comparaison-chonologique') != null) {
            buildChartComparaisonChronologique (
                document.querySelector('#chart-comparaison-chonologique'),
                result.seriesOperationsParMois,
                parameters
            );
        }

        if(document.querySelector('#chart-historique') != null && document.querySelector("#chart-historique-selecteur") != null) {
            buildChartHistorique (
                document.querySelector('#chart-historique'),
                document.querySelector("#chart-historique-selecteur"),
                result.seriesOperationsHistorique,
                parameters
            );
        }

        if(document.querySelector('#chart-direction-regional') != null) {
            const firstSelector = document.querySelector("#debut-select-year-departement");
            const secondSelector = document.querySelector("#fin-select-year-departement");
            setTimeout(() => {
                buildChartPourcentage (
                    document.querySelector('#chart-direction-regional'),
                    data,
                    parameters,
                    firstSelector.value,
                    secondSelector.value
                );
            }, 0);
            SelecteurParAnnees (
                firstSelector,
                secondSelector,
                [{
                    "nom": updateChartPourcentage,
                    "args": []
                }]
            );
        }

        if(document.querySelector('#indicateurs-departementaux') != null) {
            const firstSelector = document.querySelector("#debut-select-year-departement");
            const secondSelector = document.querySelector("#fin-select-year-departement");
            mapOperationsParAnneesParDepartement (
                data,
                document.querySelector("#debut-select-year-departement").value,
                document.querySelector("#fin-select-year-departement").value
            );
            SelecteurParAnnees (
                firstSelector,
                secondSelector,
                [{
                    "nom": mapOperationsParAnneesParDepartement,
                    "args": [data]
                }]
            );
        }




        // ==================== Géographie: région
        if(document.querySelector('#chart-comparaison-geographique-region') != null) {
            buildChartComparaisonGeographiqueRegion (
                document.querySelector('#chart-comparaison-geographique-region'),
                result.seriesOperationsParRegionsComparaisonMensuelle,
                parameters,
            );
        }

        if(document.querySelector('#carte-france-region-classement') != null) {
            fillMapPourcent (
                document.querySelector('#carte-france-region-classement'),
                "carte-reg",
                result.pourcentageRegionsAnneeEnCours.data,
                parameters["greenColor"]
            );
        }

        if(document.querySelector('#chart-cumul-mois-region') != null) {
            buildChartCumulMoisRegion (
                document.querySelector('#chart-cumul-mois-region'),
                result.regionsCumulMensuel,
                parameters,
            );
        }

        if(document.querySelector('#carte-france-region-selecteur') != null) {
            initSelectorMap (
                document.querySelector('#carte-france-region-selecteur'),
                "carte-reg",
                result.regionsCumulMensuel.data.codes,
                result.regionsCumulMensuel.data.previous,
                [
                    updateChartComparaisonRegion,
                    updateChartCumulMoisRegion
                ]
            );
        }


        // ==================== Géographie: département
        if(document.querySelector('#chart-comparaison-geographique-departement') != null) {
            buildChartComparaisonGeographiqueDepartement (
                document.querySelector('#chart-comparaison-geographique-departement'),
                result.seriesOperationsParDepartementsComparaisonMensuelle,
                parameters,
            );
        }

        if(document.querySelector('#carte-france-departement-classement') != null) {
            fillMapPourcent (
                document.querySelector('#carte-france-departement-classement'),
                "carte-dep",
                result.pourcentageDepartementsAnneeEnCours.data,
                parameters["greenColor"]
            );
        }

        if(document.querySelector('#chart-cumul-mois-departement') != null) {
            buildChartCumulMoisDepartement (
                document.querySelector('#chart-cumul-mois-departement'),
                result.departementsCumulMensuel,
                parameters,
            );
        }

        if(document.querySelector('#carte-france-departement-selecteur') != null) {
            initSelectorMap (
                document.querySelector('#carte-france-departement-selecteur'),
                "carte-dep",
                result.departementsCumulMensuel.data.codes,
                result.departementsCumulMensuel.data.previous,
                [
                    updateChartComparaisonDepartement,
                    updateChartCumulMoisDepartement
                ]
            );
        }
        
        


        document.querySelector("#loader-mask").remove();
    },
    error: function(error){
        document.body.innerHTML = error.responseText;
    }
});