"use strict"


// ======================================== IMPORTS
import { buildChartComparaisonGeographiqueDepartement, updateChartComparaisonDepartement } from "../../charts/comparaisonGeographique.js"
import { buildChartCumulMoisDepartement, updateChartCumulMoisDepartement } from "../../charts/cumul.js"
 import { fillMapPourcent } from "../../maps/fillMap.js"
 import { initSelectorMap } from "../../maps/selectorMap.js"
import { parameters } from "../../charts/parameters.js"




 // ======================================== REQUEST
$.ajax({
    url: "data/",
    type: "GET",
    dataType: "json",
    Origin: "0.0.0.0:5000/",
    success: function( result ){

        try {
            buildChartComparaisonGeographiqueDepartement (
                document.querySelector('#chart-comparaison-geographique-departement'),
                result.seriesOperationsParDepartementsComparaisonMensuelle,
            );
        } catch (error) {
            console.log(error);
        }
            

        try {
            fillMapPourcent (
                document.querySelector('#carte-france-departement-classement'),
                "carte-dep",
                result.pourcentageDepartementsAnneeEnCours.data,
                parameters["greenColor"]
            );
        } catch (error) {
            console.log(error);
        }


        try {
            buildChartCumulMoisDepartement (
                document.querySelector('#chart-cumul-mois-departement'),
                result.departementsCumulMensuel,
            );
        } catch (error) {
            console.log(error);
        }


        try {
            initSelectorMap (
                document.querySelector('#carte-france-departement-selecteur'),
                "carte-dep",
                result.departementsCumulMensuel.data.codes,
                result.departementsCumulMensuel.data.previous,
                [
                    updateChartComparaisonDepartement,
                    updateChartCumulMoisDepartement
                ]
            );
        } catch (error) {
            console.log(error);
        }

        document.querySelector("#loader-mask").remove();
    },
    error: function(error){
        document.body.innerHTML = error.responseText;
    }
});