"use strict"


// ======================================== IMPORTS
import { buildChartComparaisonGeographiqueRegion, updateChartComparaisonRegion } from "../../charts/comparaisonGeographique.js"
import { buildChartCumulMoisRegion, updateChartCumulMoisRegion } from "../../charts/cumul.js"
import { fillMapPourcent } from "../../maps/fillMap.js"
import { initSelectorMap } from "../../maps/selectorMap.js"
import { parameters } from "../../charts/parameters.js"




 // ======================================== REQUEST
$.ajax({
    url: "data/",
    type: "GET",
    dataType: "json",
    Origin: "0.0.0.0:5000/",
    success: function( result ){

        try {
            buildChartComparaisonGeographiqueRegion (
                document.querySelector('#chart-comparaison-geographique-region'),
                result.seriesOperationsParRegionsComparaisonMensuelle,
            );
        } catch (error) {
            console.log(error);
        }
            

        try {
            fillMapPourcent (
                document.querySelector('#carte-france-region-classement'),
                "carte-reg",
                result.pourcentageRegionsAnneeEnCours.data,
                parameters["greenColor"]
            );
        } catch (error) {
            console.log(error);
        }


        try {
            buildChartCumulMoisRegion (
                document.querySelector('#chart-cumul-mois-region'),
                result.regionsCumulMensuel,
            );
        } catch (error) {
            console.log(error);
        }


        try {
            initSelectorMap (
                document.querySelector('#carte-france-region-selecteur'),
                "carte-reg",
                result.regionsCumulMensuel.data.codes,
                result.regionsCumulMensuel.data.previous,
                [
                    updateChartComparaisonRegion,
                    updateChartCumulMoisRegion
                ]
            );
        } catch (error) {
            console.log(error);
        }

        document.querySelector("#loader-mask").remove();
    },
    error: function(error){
        document.body.innerHTML = error.responseText;
    }
});