"use strict"


/**
 * Transforme le tableau en tableau de données.
 * Celà inclus la pagination, le nombre de lignes visible
 * par page ainsi qu'une barre de recherche.
 * 
 * libs: DataTables
 */

$(document).ready( function () {
    $('#table').DataTable({
        scrollX: false,
        scrollCollapse: true
    });
    document.querySelector("#loader-mask").remove();
} );