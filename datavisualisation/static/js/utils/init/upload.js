"use strict"


// ======================================== VARIABLES
let fileNameBalise = document.querySelectorAll(".upload-file .upload-file-name");




// ======================================== EVENT
/**
 * Initialise les zones de chargement pour permetre un affichage du nom et
 * de la taille du fichier à chaque chnagement de valeur dans l'input.
 */
window.onload = (event => {
    for(let balise of fileNameBalise){
        let idTarget = balise.getAttribute("target");
        let target = document.querySelector("#" + idTarget);
        target.addEventListener("change", (event => {
            let filesize = target.files[0].size / 1000000;
            balise.textContent = target.files[0].name + " (" + filesize.toFixed(2) + "Mo)";
        }))
    }
});