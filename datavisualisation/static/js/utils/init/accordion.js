"use strict"


// ======================================== VARIABLES
let listAccordion = document.querySelectorAll(".accordion > a.nav-link");




// ======================================== EVENT
/**
 * Initialise les zones de chargement pour permetre un affichage du nom et
 * de la taille du fichier à chaque chnagement de valeur dans l'input.
 */
window.onload = (event => {
    for(let accordion of listAccordion){
        accordion.addEventListener("click", event =>{
            accordion.lastElementChild.classList.toggle("rotate-90");
        });
    }
});