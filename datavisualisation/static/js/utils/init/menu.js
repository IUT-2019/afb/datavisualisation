"use strict"


// ======================================== EVENT
/**
 * Initialise la sélection de l'item du menu en adéquation avec l'url.
 */
window.onload = (event => {
    const menu = document.querySelector(".sidebar > #menu"); // La liste des liens
    const url = window.location.pathname; // l'url de la page actuel
    let active = false;
    let i = 0;
    while( !active &&  i < menu.childElementCount){
        let link = menu.children[i].firstElementChild;
        if( link != null && link.nodeName == "A" && link.pathname == url ){
            menu.children[i].firstElementChild.classList.add("active");
            active = true;

            // Si accordéon, je le déplis
            const dataParent = link.parentElement.getAttribute("data-parent");
            const accordionParent = document.querySelector( dataParent );
            if( accordionParent != null ){
                accordionParent.setAttribute("aria-expanded", true);
                accordionParent.classList.remove("collapsed");
            }
            const accordionChildrens = document.querySelectorAll( "[data-parent='" + dataParent + "']" );
            for( let children of accordionChildrens ){
                children.classList.add("show");
            }
        }
        i += 1;
    }
});