"use strict"


/**
 * Colorie une carte qui n'est pas dédié à la sélection.
 * @param {balise} map la balise "object"
 * @param {string} idDebut ex: carte-reg, pour les régions de france
 * @param {Array} classes la liste de liste de code dans l'ordre croissant
 * @param {string} color la couleur sur lequel on réalise le dégradé
 */
export async function fillMap(map, idDebut, classes, color){

    const opacities = await extrapolation( classes.length );
    let cpt = 0;
    for( let classe of classes ){
        for( let code of classe ){
            let elem = map.contentDocument.documentElement.querySelector( "#" + idDebut + "-" + code )
            elem.style.fill = color;
            elem.style.fillOpacity = opacities[cpt];
        }
        cpt += 1;
    }
}




/**
 * Détermine une graduation entre 0 et 1 en fonction de la taille des données
 * à afficher sur la carte. 
 * @param {number} size la taille des données
 */
function extrapolation( size ){
    return new Promise( resolve => {
        let value = new Array();
        const unit = 1 / (size + 4);
        for(let i=0; i<size; i++){
            value.push( 1 - unit * i );
        }
        resolve( value );
    });
}




export async function fillMapPourcent(map, idDebut, pourcents, color){

    const maxValue = Math.max( ...Object.values(pourcents) );
    for( let code in pourcents ){
        let elem = map.contentDocument.documentElement.querySelector( "#" + idDebut + "-" + code )
        elem.style.fill = color;
        elem.style.fillOpacity = (pourcents[code] * 100 / maxValue) / 100;
    }
}