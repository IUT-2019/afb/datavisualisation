"use strict"


// ======================================== IMPORTS
import { departements as referenceDepartements } from "../utils/references/departements.js";




/**
 * 
 * @param {Object} data 
 * @param {numeric} startyear 
 * @param {numeric} endyear 
 */
export function mapOperationsParAnneesParDepartement(data, startyear, endyear){
    clearMap();
    let value = calculate(data, startyear, endyear);
    let opacity = extrapolation(value.length);
    let cpt = 0;
    value.forEach( groupe => {
        groupe.forEach( code => {
            let departement = document.querySelector("#indicateurs-departementaux").contentDocument.querySelector("#carte-dep-" + code);
            if(departement != null){
                departement.style.fill = "#77c476";
                departement.style.fillOpacity = opacity[cpt];
            }
        });
        cpt += 1;
    });
}




/**
 * 
 * @param {Object} data 
 * @param {numeric} startyear 
 * @param {numeric} endyear
 * @returns {Array} 
 */
function calculate(data, startyear, endyear){
    let departements = new Object();
    let temporary = new Object();
    let value = new Object();
    for(let id in data){
        let year = new Date( data[id]["date"] ).getFullYear().toString();
        let code = data[id]["code_departement"];
        if(code.length == 1){
            code = "0" + code;
        }
        if(startyear<=year && year<=endyear){
            if( !(code in departements) ){
                departements[code] = 0;
            }
            departements[code] += 1;
        }
    }
    for(let code in departements){
        if( !(departements[code] in temporary) ){
            temporary[departements[code]] = new Array();
        }
        temporary[departements[code]].push(code);
    }
    value = Object.values(temporary).reverse();
    setTimeout(fillTableDepartemental, 0, value, departements);
    return value;
}




/**
 * 
 * @param {numeric} size
 * @returns {Array} 
 */
function extrapolation(size){
    let value = new Array();
    let unit = 1 / (size + 4);
    for(let i=0; i<size; i++){
        value.push( 1 - unit * i );
    }
    return value;
}




/**
 * 
 */
function clearMap(){
    let listdepartements = document.querySelector("#indicateurs-departementaux").contentDocument.querySelector("#carte-departement").children;
    for(let departement of listdepartements){
        departement.style = null;
    }
}




/**
 * 
 * @param {Array} classement 
 * @param {Object} departements 
 */
function fillTableDepartemental(classement, departements){
    let tablecontent = document.querySelector("#table-division-year-by-departement > tbody");
    tablecontent.innerHTML = null;
    let cpt = 1;
    classement.forEach( group => {
        group.forEach( code => {
            let lign = document.createElement("tr");
            let place = document.createElement("th");
            let name = document.createElement("td");
            let value = document.createElement("td");
            place.setAttribute("scope", "row");
            place.textContent = cpt;
            try {
                name.textContent = referenceDepartements[code]["nom"];
            } catch (error) {
                name.textContent = "NON RENSEIGNE"
                name.style.color = "var(--red-color)";
            }
            value.textContent = departements[code];
            lign.appendChild(place);
            lign.appendChild(name);
            lign.appendChild(value);
            tablecontent.appendChild(lign);
        });
        cpt += 1;
    });
}