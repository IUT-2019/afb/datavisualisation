"use strict"


export async function initSelectorMap(map, idDebut, active, selected, functions){

    // Active
    for(let code of active){
        let elem = map.contentDocument.documentElement.querySelector( "#" + idDebut + "-" + code );
        if( elem != null ){
            elem.classList.add( "active" );
            elem.addEventListener("click", event => {
                for(let f of functions){
                    f( code );
                }
            });
        }
    }

    // Selected
    let elem = map.contentDocument.documentElement.querySelector( "#" + idDebut + "-" + selected );
    elem.classList.add( "selected" );
}