
#### IMPORT #################
from .app import app, db
from .models import addData, getAdministrateurByName, getAllAdministrateur, addAdministrateur, Administrateur
import click




#### METHODS ################
@app.cli.command()
def db_create():
    """
        Création de toutes les tables dans la base de données.
    """
    db.create_all()




@app.cli.command()
def db_clear():
    """
        Supprime toutes les tables de la base de données.
    """
    db.drop_all()




@app.cli.command()
def db_load():
    """
        Charge dans la tables un jeu de données.
    """
    # DONNEES CSV
    addData(titre="Oison", nom="oison.csv", package="oison.jar", description="Observations naturalistes")
    addData(titre="BDOE", nom="bdoe.csv", package="bdoe.jar", description="Informations métiers sur les obstacles à l'écoulement.")

    # ADMINISTRATEUR
    addAdministrateur(login="admin_afb", password="password_afb")




@app.cli.command()
@click.option("--delete", "-d", nargs=1, help="Supprime l'administrateur via sont identifiant.")
@click.option("--add", "-a", nargs=2, help="Ajoute un nouvel administreteur.")
@click.option("--list", "-l", nargs=0, help="Liste les administrateurs.")
def admin(delete, add, list):
    """
        Gestion des administrateurs.
    """
    if delete:
        admin = getAdministrateurByName(delete)
        if( admin ):
            if( admin.delete() ):
                print("[ OK ] Suppression de l'administrateur", "\"" + delete + "\"")
            else:
                print("[ ERROR ] Echec lors de la suppression de", "\"" + delete + "\"")
        else:
            print("[ ERROR ] Aucun administrateur de correspond à", "\"" + delete + "\"")
    if add:
        username, password = add
        if( len(username) >= 8 ):
            if( len(password) >= 8 ):
                admin = addAdministrateur(login=username, password=password)
                if( not admin ):
                    print("[ ERROR ] Echec d'enregistrement d'un nouvel admin.")
                else:
                    print("[ OK ] Enregistrement terminé.")
            else:
                print("[ ERROR ] Le mot de passe doit contenir minimum 8 caractères")
        else:
            print("[ ERROR ] L'identifiant doit contenir minimum 8 caractères")
    else:
        admins = getAllAdministrateur()
        if( admins ):
            cpt = 0
            for admin in admins:
                print(admin.login, end="\t")
                cpt += 1
            print()
            print("Nombre d'administrateur:", cpt)
