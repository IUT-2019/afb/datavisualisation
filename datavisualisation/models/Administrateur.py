

#### IMPORT #################
from ..app import db
from flask_login import UserMixin
from hashlib import sha256




#### CLASS ##################
class Administrateur( db.Model ):
    id          = db.Column(db.Integer, primary_key=True, autoincrement=True)
    login       = db.Column(db.String(50), nullable=False, unique=True)
    password    = db.Column(db.String(250), nullable=False, unique=True)



    #### OBJECT METHODS
    def __repr__( self ):
        """
            Représente l'objet dans un format compréhensible pour les affichages.
            :param self: Une occurence de la table
            :type self: Object
            :return: Une représentation de l'objet
            :rtype: String
        """
        return "<Administrateur (%d) %s>" % (self.id, self.login)
    

    def delete( self ):
        """
            Supprime dans la BDD l'occurence (ligne de la table) courante.
            :param self: Une occurence de la table
            :type self: Object
            :return: Un indicateur de bonne execution de la méthode
            :rtype: Boolean
        """
        try:
            db.session.delete( self )
            db.session.commit()
            return True
        except:
            return False
    
    def is_active():
        return True
    
    def get_id(self):
        return self.id
    
    def is_authenticated():
        return True
    
    def is_anonymous():
        return False




#### TABLE METHODS ##########
def getAllAdministrateur():
    """
        Renvoie toutes les valeurs de la table.
        :return: La liste des occurences présentes dans la table
        :rtype: List
    """
    return Administrateur.query.all()


def getAdministrateur( id ):
    """
        Renvoie la ligne de la table correspondant à l'identifiant.
        :param id: L'identifiant de l'occurence
        :type id: int
        :return: L'occurence de la table qui a pour identifiant la valeur de la variable passé en paramètre
        :rtype: Object
    """
    return Administrateur.query.get( id )


def getAdministrateurByName( login ):
    """
        Renvoie la ligne de la table correspondant au login.
        :param login: L'identifiant utilisé pour se connecter
        :type login: string
        :return: L'occurence de la table qui a pour login la valeur de la variable passé en paramètre
        :rtype: Object
    """
    return Administrateur.query.filter_by(login=login).first()


def addAdministrateur(login, password):
    """
        Enregistre une nouvelle occurence dans la base de données.
        :param login: L'identifiant utilisé par l'admin.
        :param password: Le mot de passe de l'admin (en claire)
        :type login: string
        :type password: string
        :return: Un indicateur de bonne execution de la méthode
        :rtype: Boolean
    """
    try:
        mdp = sha256( password.encode() ).hexdigest()
        admin = Administrateur(login=login, password=mdp)
        db.session.add( admin )
        db.session.commit()
        return True
    except:
        return False