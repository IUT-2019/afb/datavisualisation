

#### IMPORT #################
from ..app import db
from datetime import date




#### CLASS ##################
class Data( db.Model ):
    id          = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date        = db.Column(db.DateTime)
    titre       = db.Column(db.String(250), nullable=False, unique=True)
    nom         = db.Column(db.String(250), nullable=False, unique=True)
    package     = db.Column(db.String(250), nullable=False, unique=True)
    template    = db.Column(db.String(250), nullable=False, unique=True)
    description = db.Column(db.String(250), nullable=True)



    #### OBJECT METHODS
    def __repr__( self ):
        """
            Représente l'objet dans un format compréhensible pour les affichages.
            :param self: Une occurence de la table
            :type self: Object
            :return: Une représentation de l'objet
            :rtype: String
        """
        return "<Data (%d) %s>" % (self.id, self.titre)
    

    def delete( self ):
        """
            Supprime dans la BDD l'occurence (ligne de la table) courante.
            :param self: Une occurence de la table
            :type self: Object
            :return: Un indicateur de bonne execution de la méthode
            :rtype: Boolean
        """
        try:
            db.session.delete( self )
            db.session.commit()
            return True
        except:
            print( "[ ERROR ] Suppression du jeu de données" )
            return False




#### TABLE METHODS ##########
def getAllData():
    """
        Renvoie toutes les valeurs de la table.
        :return: La liste des occurences présentes dans la table
        :rtype: List
    """
    return Data.query.all()


def getData( id ):
    """
        Renvoie la ligne de la table correspondant à l'identifiant.
        :param id: L'identifiant de l'occurence
        :type id: int
        :return: L'occurence de la table qui a pour identifiant la valeur de la variable passé en paramètre
        :rtype: Object
    """
    return Data.query.get( id )


def addData(titre, nom, package, description=None):
    """
        Enregistre une nouvelle occurence dans la base de données.
        :param titre: Le titre de la base de données
        :param nom: Le chemin d'accès
        :param package: le nom du package de mapping en JAVA
        :param description: Une courte description sur la base de données
        :type titre: String
        :type nom: String
        :type package: String
        :type description: String
        :return: Un indicateur de bonne execution de la méthode
        :rtype: Boolean
    """
    template = nom.split(".")[0]
    try:
        data = Data(date=date.today(), titre=titre, nom=nom, package=package, template=template, description=description)
        db.session.add( data )
        db.session.commit()
        return True
    except:
        print( "[ ERROR ] Enregistrement d'un nouveau jeu de données" )
        return False


def updateData( id ):
    """
        Renvoie toutes les valeurs de la table.
        :return: La liste des occurences présentes dans la table
        :rtype: List
    """
    data = getData( id )
    try:
        data.date = date.today()
        return True
    except:
        print( "[ ERROR ] Mise à jour d'un jeu de données" )
        return False