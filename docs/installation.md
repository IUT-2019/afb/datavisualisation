# Installation


## Prérequis
Si vous possédez un environnement Linux, vous pouvez passer directement à l'étape suivante. La suite de cette partie concerne uniquement les utilisateurs d'un environnement Windows.
Vous devez vous assurer d'avoir au préalable le logiciel `Python3` et `Git Bash`. Dans le cas contraire, voir les liens ci-dessous.


## Récupérer le projet 
Il faut récupérer le projet qui est hébergé sur un serveur distant sur le GitLab.
Dans le répertoire courant, ouvrez un terminal ou le git Bash pour ceux qui sont sur Windows et exécutez la commande suivante.
```shell
$ git clone https://gitlab.com/IUT-2019/afb/datavisualisation.git
```


## Création d'un virtualenv
Une fois, le projet téléchargé en local, déplacez vous dans le répertoire du projet et créez un environnement virtuel python.
```shell
$ cd datavisalisation
$ virtualenv venv
```
Dans le cas ou vous ne parvenez pas à créer votre venv, installez la commande `virtualenv`.
```shell
$ pip install virtualenv
```

[liens]:[1]
## Liens
- Python3 ( [lien](https://www.python.org/downloads/, "Télécharger Python3") )
- Git Bash ( [lien](https://gitforwindows.org/, "Télécharger Git Bash") )