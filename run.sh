# Script de démarage du serveur


# Activation de l'environnement virtuel
source venv/Scripts/activate

# Lancement de l'application
# host=0.0.0.0 : Ecoute sur le broadcast pour accepter toutes les machines.
# port=5000 : Ecoute et dicute sur le port 5000.
flask run --host=0.0.0.0 --port=5000